<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body class="body-carritoCompras">

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <div class="carritoCompras">
            <!-- Start Carrito de Compras -->
            <section class="carritoCompras--section">
                <h3 class="h3 color-internas wow fadeInDown" data-wow-duration="1.1s" data-wow-delay="0s">Carrito de compras</h3>
                <p class="cantidadMobile text-may">03 productos</p>
                <div class="tableCarrito wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.2s">
                    <div class="tableCarrito-head">
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image text-center">
                                <span class="text-may">Imagen</span>
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <span class="text-may">Producto</span>
                                </div>
                                <div class="cantidad text-center">
                                    <span class="text-may">Cantidad</span>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="text-may">Precio Unitario</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="text-may">Subtotal</span>
                                </div>
                                <div class="borrar text-center">
                                    <span class="text-may">Borrar</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tableCarrito-body">
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image">
                                <img class="img-responsive" src="assets/images/carrito/producto-02.jpg" alt="Producto">
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <p class="color-internas">Parlantes Xtreme Usb (Kes-215A)</p>
                                    <span class="color-primary">Código: 137295</span>
                                </div>
                                <div class="cantidad">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="color-internas">S/ 69.00</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="color-internas">S/ 138.00</span>
                                </div>
                                <div class="borrar text-center">
                                    <button type="button" class="btn-borrar" name="button">
                                        <i class="icon-tachito"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image">
                                <img class="img-responsive" src="assets/images/carrito/producto-04.jpg" alt="Producto">
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <p class="color-internas">Tomatodo para batidos</p>
                                    <span class="color-primary">Código: 137295</span>
                                </div>
                                <div class="cantidad">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="color-internas">S/ 7.00</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="color-internas">S/ 14.00</span>
                                </div>
                                <div class="borrar text-center">
                                    <button type="button" class="btn-borrar" name="button">
                                        <i class="icon-tachito"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image">
                                <img class="img-responsive" src="assets/images/carrito/producto-05.jpg" alt="Producto">
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <p class="color-internas">Boys Pique Polo</p>
                                    <span class="color-primary">Código: 137295</span>
                                </div>
                                <div class="cantidad">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="color-internas">S/ 25.00</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="color-internas">S/ 150.00</span>
                                </div>
                                <div class="borrar text-center">
                                    <button type="button" class="btn-borrar" name="button">
                                        <i class="icon-tachito"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid">
                    <div class="box-form col-3 wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.4s">
                        <h4 class="h4 text-may color-internas">Datos del comprador</h4>
                        <form class="form" action="#" method="post">
                            <div class="form__wrapper">
                                <input type="email" class="form__input bg-input" id="email" name="email">
                                <label class="form__label">
                                    <span class="form__label-content">E-mail</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="apellidos" name="apellidos">
                                <label class="form__label">
                                    <span class="form__label-content">Apellidos</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="nombres" name="nombres">
                                <label class="form__label">
                                    <span class="form__label-content">Nombres</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <select class="form__select bg-input">
                                    <option selected>Tipo de Documento</option>
                                    <option value="1">DNI</option>
                                    <option value="2">Pasaporte</option>
                                    <option value="3">Carnet de Extranjería</option>
                                    <option value="4">Otros</option>
                                </select>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="dni" name="dni">
                                <label class="form__label">
                                    <span class="form__label-content"># Documento</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <input type="tel" class="form__input bg-input" id="telefono" name="telefono">
                                <label class="form__label">
                                    <span class="form__label-content">Teléfono</span>
                                </label>
                            </div>
                        </form>
                    </div>
                    <div class="box-form col-3 wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.6s">
                        <h4 class="h4 text-may color-internas">Datos de entrega</h4>
                        <form class="form" action="#" method="post">
                            <h5 class="h5 color-internas">¿Quién lo recibe?</h5>
                            <div class="radio-inline">
                                <div class="radio">
                                    <input type="radio" id="yo" name="recepcion" checked>
                                    <label for="yo">Yo</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="otraPersona" name="recepcion">
                                    <label for="otraPersona">Otra persona</label>
                                </div>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="nombresApellidos" name="nombresApellidos">
                                <label class="form__label">
                                    <span class="form__label-content">Nombres y Apellidos</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <select class="form__select bg-input">
                                    <option selected>Distrito</option>
                                    <option value="1">Ancón</option>
                                    <option value="2">Ate</option>
                                    <option value="3">Barranco</option>
                                    <option value="4">Breña</option>
                                </select>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="direccion" name="direccion">
                                <label class="form__label">
                                    <span class="form__label-content">Dirección</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="referencia" name="referencia">
                                <label class="form__label">
                                    <span class="form__label-content">Referencia</span>
                                </label>
                            </div>
                            <div class="form__wrapper">
                                <textarea class="form_textarea bg-input" id="observaciones"
                                    name="observaciones"></textarea>
                                <label class="form__label">
                                    <span class="form__label-content">Observaciones</span>
                                </label>
                            </div>
                            <h5 class="h5 color-internas">Tipo de entrega:</h5>
                            <div class="radio-inline">
                                <div class="radio">
                                    <input type="radio" id="delivery" name="tipoEntrega" checked>
                                    <label for="delivery">Delivery</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="enTienda" name="tipoEntrega">
                                    <label for="enTienda">Recojo en Tienda</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-form col-3 wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0.8s">
                        <h4 class="h4 text-may color-internas">Fecha de entrega</h4>
                        <div id="datepicker-carrito" class="datepicker"></div>
                        <form class="form" action="#" method="post">
                            <h5 class="h5 color-internas">Horario de entrega:</h5>
                            <div class="form__wrapper">
                                <select class="form__select bg-input">
                                    <option selected>Selecciona un horario</option>
                                    <option value="1">8:00 am - 10:00 am</option>
                                    <option value="2">11:00 am - 12:00 pm</option>
                                    <option value="3">15:00 pm - 17:00 pm</option>
                                    <option value="4">18:00 pm - 20:00 pm</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="grid">
                    <div class="box-form col-2 wow fadeInLeft" data-wow-duration="1.1s" data-wow-delay="0s">
                        <form class="form" action="#" method="post">
                            <h5 class="h5 color-internas">Comprobante de Pago</h5>
                            <div class="radio-inline">
                                <div class="radio">
                                    <input type="radio" id="boleta" name="comprobante" checked>
                                    <label for="boleta">Boleta</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="factura" name="comprobante">
                                    <label for="factura">Factura</label>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="form__wrapper col-2">
                                    <input type="text" class="form__input bg-input" id="razonSocial" name="razonSocial">
                                    <label class="form__label">
                                        <span class="form__label-content">Razón Social</span>
                                    </label>
                                </div>
                                <div class="form__wrapper col-2">
                                    <input type="text" class="form__input bg-input" id="ruc" name="ruc">
                                    <label class="form__label">
                                        <span class="form__label-content">RUC</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="direccion" name="direccion">
                                <label class="form__label">
                                    <span class="form__label-content">Dirección</span>
                                </label>
                            </div>
                        </form>
                    </div>
                    <div class="box-form col-2 wow fadeInRight" data-wow-duration="1.1s" data-wow-delay="0s">
                        <form class="form" action="#" method="post">
                            <h5 class="h5 color-internas">Formas de pago:</h5>
                            <div class="radio-inline">
                                <div class="radio">
                                    <input type="radio" id="tarjeta" name="formasPago" checked>
                                    <label for="tarjeta">Tarjeta</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="depositoTransferencia" name="formasPago">
                                    <label for="depositoTransferencia">Depósito Transferencia Bancaria</label>
                                </div>
                            </div>
                            <div class="cupon-descuento">
                                <h5 class="h5 color-internas">Cupón de descuento:</h5>
                                <div class="form__wrapper">
                                    <input type="text" class="form__input bg-input" id="cupon" name="cupon">
                                    <label class="form__label">
                                        <span class="form__label-content">Ingresar código</span>
                                    </label>
                                    <button type="button" class="btn-validar text-may color-primary" name="button">Validar</button>
                                    <button type="button" class="btn-borrar" name="button">
                                        <i class="icon-tachito"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <!-- End Carrito de Compras -->
            <!-- Start Total a Pagar -->
            <aside class="carritoCompras--aside text-center tramado">
                <div id="sticky-pagar" class="stickyPagar">
                    <h3 class="h3 color-internas">Total a pagar</h3>
                    <p class="cantidad">(3 productos)</p>
                    <p class="precioDelivery">
                        <i class="icon-truck"></i>
                        <span>Delivery: S/ 50.00</span>
                    </p>
                    <div class="precioTotal bg-white">
                        <span class="color-primary">S/ 464.00</span>
                    </div>
                    <div class="checkbox terminos-politica">
                        <label class="label-pol">
                            <input type="checkbox"/><i class="helper"></i>
                            <span>He leído y acepto los <a href="terminos-y-condiciones.php" class="span-pol titles-int color-internas">Términos y Condiciones</a> y la <a href="politicas-de-privacidad.php" class="span-pol titles-int color-internas">Política de Privacidad.</a></span>
                        </label>
                    </div>
                    <button onclick="window.location='compra-realizada.php'" type="button" class="btn btn-red text-may" name="button">Pagar</button>
                    <a href="index.php" class="btn btn-seguir underlineHover color-primary">Seguir comprando</a>
                </div>
            </aside>
            <!-- End Total a Pagar -->
        </div>
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <script type="text/javascript">
        /* === Total a Pagar - Fixed === */
        if ($(window).width() > 768) {
            $(window).scroll(function () {
                if ($(window).scrollTop() > 133) { // 133 = Height del Header
                    $('#sticky-pagar').css('position', 'fixed');
                    $('#sticky-pagar').css('top', '133px');
                }

                else if ($(window).scrollTop() <= 389) { // 389 = Height del Footer
                    $('#sticky-pagar').css('position', '');
                    $('#sticky-pagar').css('top', '');
                }
                if ($('#sticky-pagar').offset().top + $("#sticky-pagar").height() > $("#footer").offset().top) {
                    $('#sticky-pagar').css('top', -($("#sticky-pagar").offset().top + $("#sticky-pagar").height() - $("#footer").offset().top));
                }
            });
        }
    </script>
    <!-- End Scripts -->

</body>
</html>
