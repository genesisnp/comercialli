<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- PERFIL DE USUARIO -->
    <main class="main-perfil">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="color-primary titles-big text-center ttl-mp">MI PERFIL</h2>
                </div>
                <ul class="nav nav-tabs tabs col-xs-12 col-md-8 col-lg-2">
                    <li class="active">
                        <a href="#tab-1" class="tab-link p-internas" aria-controls="tab1" role="tab" data-toggle="tab">MIS DATOS</a>
                    </li>
                    <li>
                        <a href="#tab-2" class="tab-link p-internas" aria-controls="tab2" role="tab" data-toggle="tab">MIS COMPRAS</a>
                    </li>
                </ul>
                <div class="col-xs-12 col-lg-9">
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active col-xs-12 col-sm-9 col-lg-7">
                            <form action="#" class="row form" method="post">
                                <div class="col-xs-12 form-dat-usu">
                                    <div class="row d-alig-flex flex-column">
                                        <div class="col-xs-11 titles-big ttl-tabs-perfil color-internas">DATOS DE USUARIO</div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="name-contac" name="name-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Nombre:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="last-name-contac" name="last-name-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Apellidos:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="dni-contac" name="dni-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Documento de identidad:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="cel-contac" name="cel-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Celular:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="email" class="form__input bg-input" id="phone-contac" name="phone-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Teléfono:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="email" class="form__input bg-input" id="email-contac" name="email-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">correo:</span>
                                            </label>
                                        </div>
                                        <div class="col-xs-11">
                                            <div class="checkbox">
                                                <label class="label-pol">
                                                    <input type="checkbox" /><i class="helper"></i>
                                                    <span>Usted reconoce haber leído la Política de 
                                                        <a href="politicas-de-privacidad.php" class="span-pol titles-int color-primary">protección de datos</a> personales.</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 pd-x-0">
                                    <button type="submit" class="btn-send btn-red titles-int "
                                            id="btn-send-form">ACTUALIZAR</button>
                                </div>
                                <div class="col-xs-12 form-dat-usu">
                                    <div class="row d-alig-flex flex-column">
                                        <div class="col-xs-11 titles-big ttl-tabs-perfil color-internas">CAMBIAR CONTRASEÑA</div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="name-contac" name="name-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Contraseña Actual:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="name-contac" name="name-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Contraseña nueva:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-11">
                                            <input type="text" class="form__input bg-input" id="name-contac" name="name-contac">
                                            <label class="form__label">
                                                <span class="form__label-content">Repetir Contraseña:</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 pd-x-0">
                                    <button type="submit" class="btn-send btn-red titles-int "
                                            id="btn-send-form">GUARDAR CAMBIOS</button>
                                </div>
                            </form>
                        </div>
                        <div id="tab-2" class="tab-pane col-xs-12 col-md-11 col-lg-9">
                            <div class="content-mis-compras">
                                <h2 class="p-internas color-primary text-uppercase text-center">HOLA, JUAN PEREZ</h2>
                                <p class="p-internas text-center">Bienvenido, recuerda que puedes hacerle seguimiento a tu pedido y que puedes modificar tus datos.</p>
                                <div class="tabla-compras">
                                    <div class="detall-compras">
                                        <div class="pedido">
                                            <h3 class="titles-int">PEDIDO N° 22334</h3>
                                            <span class="p-internas color-primary">02/03/2020</span>
                                        </div>
                                        <ul class="content-seg">
                                            <li class="item-seg detall-seg">
                                                <i class="icons-det icon-list"></i>
                                                <span class="p-internas">Detalles</span>
                                            </li>
                                            <li class="item-seg ped-conf">
                                                <i class="icons-det icon-check-mark"></i>
                                                <span class="p-internas">Pedido confirmado</span>
                                            </li>
                                            <li class="item-seg ped-an">
                                                <i class="icons-det icon-error-circle"></i>
                                                <span class="p-internas">Pedido anulado</span>
                                            </li>
                                            <li class="item-seg ped-ent">
                                                <i class="icons-det icon-check-mark"></i>
                                                <span class="p-internas">Pedido entregado</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="detall-compras">
                                        <div class="pedido">
                                            <h3 class="titles-int">PEDIDO N° 22334</h3>
                                            <span class="p-internas color-primary">02/03/2020</span>
                                        </div>
                                        <ul class="content-seg">
                                            <li class="item-seg detall-seg">
                                                <i class="icons-det icon-list"></i>
                                                <span class="p-internas">Detalles</span>
                                            </li>
                                            <li class="item-seg ped-conf">
                                                <i class="icons-det icon-check-mark"></i>
                                                <span class="p-internas">Pedido confirmado</span>
                                            </li>
                                            <li class="item-seg ped-an">
                                                <i class="icons-det icon-error-circle"></i>
                                                <span class="p-internas">Pedido anulado</span>
                                            </li>
                                            <li class="item-seg ped-ent">
                                                <i class="icons-det icon-check-mark"></i>
                                                <span class="p-internas">Pedido entregado</span>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
