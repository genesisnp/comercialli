<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-privacy-policies">
        <section class="banner mg-b-banner">
            <img class="img-cover" src="assets/images/banner/politica-de-privacidad.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-polit-priv color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Políticas de privacidad</h2>
            </div>
        </section>
        <section class="sct-acc-policies">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-10 float-center">
                        <ul class="accordion">
                            <li><a class="accordion-heading titles-int text-uppercase">política de protección de datos <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto quos obcaecati rerum! Laborum a asperiores modi quos obcaecati 
                                        rerum! Laborum a asperiores modi omnis quos obcaecati rerum! Laborum a asperiores modipraesentium.</p>
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">privacidad <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">derecho y protección del consumidor <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
