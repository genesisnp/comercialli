<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-camb-dev">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/cambios-y-devoluciones.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-como-cambios color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Cambios y devoluciones</h2>
            </div>
        </section>
        <section class="sct-info-cd">
            <div class="container-fluid">
                <div class="row d-alig-flex">
                    <div class="col-xs-7 pd-x-0 hidden-xs hidden-sm">
                        <div class="content-img-cd">
                            <img class="img-cover wow fadeInLeft" src="assets/images/internas/pared-devol.jpg" alt="">
                            <img class="img-devol wow zoomIn" data-wow-delay="1s" src="assets/images/internas/camb-devo.png" alt="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5">
                        <div class="content-descp-cd wow fadeInRight">
                            <p class="p-internas font-s-p1">Solo se aceptarán cambios y/o devoluciones sujeto a evaluación, 
                            si el producto llegara dañado. Las solicitudes de devolución o cambio de productos deberán 
                            coordinarse dentro del plazo de 2 días laborables a través del correo <span class="color-primary">sugerencias@comercial-li.com.</span></p>
                            <p class="p-internas font-s-p1">Pasado los 2 días laborables desde la entrega del producto ya no se podrá 
                            solicitar ningún cambio o devolución debido a que se considerará que el producto llego 
                            satisfactoriamente y se dará por concluida la transacción.</p>
                            <p class="p-internas font-s-p1">Si el producto no es retornado en su empaque original en buenas condiciones y 
                            comprobante de pago, no se procederá con el cambio.La empresa evaluará el producto verificando si tiene 
                            algún daño por manipulación del envío.En caso se quiera ejercer el derecho de devolución o cambio el 
                            cliente deberá cumplir con las siguientes normas:</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
        <section class="sct-check-cd bg-forma">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 flex-mob">
                        <div class="row d-flex-just">
                            <div class="col-xs-12 col-md-5">
                                <div class="check-cd wow zoomIn">
                                    <div class="div-icon-check"></div>
                                    <p class="p-internas font-s-p1">El producto no deberá estar dañado por acciones del cliente o terceros.</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <div class="check-cd wow zoomIn" data-wow-delay="1s">
                                    <div class="div-icon-check"></div>
                                    <p class="p-internas font-s-p1">El empaque debera estar en perfectas condiciones.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 flex-mob">
                        <div class="row d-flex-just">
                            <div class="col-xs-12 col-md-5">
                                <div class="check-cd wow zoomIn">
                                    <div class="div-icon-check"></div>
                                    <p class="p-internas font-s-p1">El producto no debe mostrar señales de uso, suciedad o desgaste.</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <div class="check-cd wow zoomIn" data-wow-delay="1s">
                                    <div class="div-icon-check"></div>
                                    <p class="p-internas font-s-p1">El costo de servicio de trasporte generado por cambios y/o devoluciones en todos nuestros 
                                    productos serán asumidos por el cliente en su totalidad.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>

