<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <!-- Start Categorías de Productos -->
        <section class="section categorias-productos">
            <div class="container">
                <!-- Start Breadcrumb -->
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Productos</span>
                    <span class="breadcrumb-item">»</span>
                    <span class="breadcrumb-item">Arte y manualidades</span>
                    <span class="breadcrumb-item">»</span>
                    <span class="breadcrumb-item active">Adhesivos y pegamentos</span>
                </div>
                <!-- End Breadcrumb -->
                <div class="row">
                    <!-- Start Categorías -->
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="categorias wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
                            <a href="javascript:void(0)" class="categorias--heading">
                                <h4 class="color-internas">Arte y manualidades</h4>
                                <div class="plus-minus-toggle collapsed">
                                    <span class="plus-minus"></span>
                                </div>
                            </a>
                            <div class="categorias--body">
                                <a href="categorias.php" class="categorias--body__item color-internas active">
                                    <span class="underlineHover">Adhesivos y pegamentos</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Artículos para fiestas</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Cerámica y pastas</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Cuadernos y blocks</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Escritura</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Herramientas para cortar</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Herramientas para pintar</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Manualidades</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Microporoso y corrospum</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Papeles</span>
                                </a>
                                <a href="categorias.php" class="categorias--body__item color-internas">
                                    <span class="underlineHover">Pinturas</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- End Categorías -->
                    <!-- Start Productos -->
                    <div class="col-xs-12 col-sm-12 col-md-10">
                        <div class="productos">
                            <div class="productos--heading">
                                <h3 class="h3 color-internas wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.2s">Adhesivos y pegamentos</h3>
                                <form class="form" action="" method="post">
                                    <div class="form__wrapper">
                                        <select class="form__select bg-input">
                                            <option selected>Ordenar por:</option>
                                            <option value="1">A-Z</option>
                                            <option value="2">Z-A</option>
                                            <option value="3">Menor precio</option>
                                            <option value="4">Mayor precio</option>
                                            <option value="5">Lo nuevo</option>
                                            <option value="6">Más vendidos</option>
                                            <option value="7">Mejor Descuento</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="productos--list">
                                <div class="grid">
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <span class="producto-image__dscto bg-primary color-white">-30%</span>
                                                <a href="detalle-producto-ofertas.php">
                                                    <img class="img-cover" src="assets/images/ofertas/producto-01.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto-ofertas.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Parlantes Xtreme Usb (Kes-215A)</h4>
                                                <div class="antes">
                                                    <span class="antes-text">Antes</span>
                                                    <span class="antes-number">S/ 80.00</span>
                                                </div>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-text">Ahora</span>
                                                    <span class="ahora-number">S/ 69.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <span class="producto-image__dscto bg-primary color-white">-15%</span>
                                                <a href="detalle-producto-ofertas.php">
                                                    <img class="img-cover" src="assets/images/ofertas/producto-02.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto-ofertas.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                                <div class="antes">
                                                    <span class="antes-text">Antes</span>
                                                    <span class="antes-number">S/ 80.00</span>
                                                </div>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-text">Ahora</span>
                                                    <span class="ahora-number">S/ 20.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <span class="producto-image__dscto bg-primary color-white">-10%</span>
                                                <a href="detalle-producto-ofertas.php">
                                                    <img class="img-cover" src="assets/images/ofertas/producto-04.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto-ofertas.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Base p/CPU (Cs003M) Ajustable c/Ruedas Y Frenos</h4>
                                                <div class="antes">
                                                    <span class="antes-text">Antes</span>
                                                    <span class="antes-number">S/ 15.00</span>
                                                </div>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-text">Ahora</span>
                                                    <span class="ahora-number">S/ 12.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <a href="detalle-producto.php">
                                                    <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-number">S/ 8.50</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <a href="detalle-producto.php">
                                                    <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-number">S/ 10.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <a href="detalle-producto.php">
                                                    <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-number">S/ 5.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <span class="producto-image__dscto bg-primary color-white">-30%</span>
                                                <a href="detalle-producto-ofertas.php">
                                                    <img class="img-cover" src="assets/images/ofertas/producto-01.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto-ofertas.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Parlantes Xtreme Usb (Kes-215A)</h4>
                                                <div class="antes">
                                                    <span class="antes-text">Antes</span>
                                                    <span class="antes-number">S/ 80.00</span>
                                                </div>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-text">Ahora</span>
                                                    <span class="ahora-number">S/ 69.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <span class="producto-image__dscto bg-primary color-white">-15%</span>
                                                <a href="detalle-producto-ofertas.php">
                                                    <img class="img-cover" src="assets/images/ofertas/producto-02.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto-ofertas.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                                <div class="antes">
                                                    <span class="antes-text">Antes</span>
                                                    <span class="antes-number">S/ 80.00</span>
                                                </div>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-text">Ahora</span>
                                                    <span class="ahora-number">S/ 20.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <span class="producto-image__dscto bg-primary color-white">-10%</span>
                                                <a href="detalle-producto-ofertas.php">
                                                    <img class="img-cover" src="assets/images/ofertas/producto-04.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto-ofertas.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Base p/CPU (Cs003M) Ajustable c/Ruedas Y Frenos</h4>
                                                <div class="antes">
                                                    <span class="antes-text">Antes</span>
                                                    <span class="antes-number">S/ 15.00</span>
                                                </div>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-text">Ahora</span>
                                                    <span class="ahora-number">S/ 12.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <a href="detalle-producto.php">
                                                    <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-number">S/ 8.50</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <a href="detalle-producto.php">
                                                    <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-number">S/ 10.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                        <div class="producto">
                                            <div class="producto-image">
                                                <a href="detalle-producto.php">
                                                    <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                                </a>

                                                <div class="product-overlay">
                                                    <a href="detalle-producto.php">Ver detalle</a>
                                                </div>
                                            </div>
                                            
                                            <div class="product-body">
                                                <h4 class="h4 color-internas">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                                <div class="ahora color-primary">
                                                    <span class="ahora-number">S/ 5.00</span>
                                                </div>
                                            </div>

                                            <div class="producto-foot">
                                                <div class="amount-buy">
                                                    <div class="number">
                                                        <span class="minus">-</span>
                                                        <input type="text" value="1"/>
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a href="" class="btn btn-red">
                                                        <i class="icon-carrito color-white"></i>
                                                        <span>Comprar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <aside class="productos--list__aside">
                                    <div id="sticky-carrito" class="carrito stickyCarrito text-center bg-white">
                                        <h3 class="color-primary">Tu carrito</h3>
                                        <p>(Tienes 3 productos)</p>
                                        <div class="carrito--item">
                                            <img class="carrito--item__image img-responsive" src="assets/images/carrito/producto-01.jpg" alt="Imagen Producto Carrito">
                                            <div class="carrito--item__caption">
                                                <div class="carrito--item__captionBox">
                                                    <h4 class="color-internas">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                                </div>
                                                <div class="carrito--item__captionBox">
                                                    <div class="amount-buy">
                                                        <div class="number">
                                                            <span class="minus">-</span>
                                                            <input type="text" value="1"/>
                                                            <span class="plus">+</span>
                                                        </div>
                                                    </div>
                                                    <span class="precio color-primary">S/ 20.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carrito--item">
                                            <img class="carrito--item__image img-responsive" src="assets/images/carrito/producto-02.jpg" alt="Imagen Producto Carrito">
                                            <div class="carrito--item__caption">
                                                <div class="carrito--item__captionBox">
                                                    <h4 class="color-internas">Parlantes Xtreme Usb (Kes-215A)</h4>
                                                </div>
                                                <div class="carrito--item__captionBox">
                                                    <div class="amount-buy">
                                                        <div class="number">
                                                            <span class="minus">-</span>
                                                            <input type="text" value="1"/>
                                                            <span class="plus">+</span>
                                                        </div>
                                                    </div>
                                                    <span class="precio color-primary">S/ 30.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carrito--item">
                                            <img class="carrito--item__image img-responsive" src="assets/images/carrito/producto-03.jpg" alt="Imagen Producto Carrito">
                                            <div class="carrito--item__caption">
                                                <div class="carrito--item__captionBox">
                                                    <h4 class="color-internas">Parlantes Xtreme Usb (Kes-215A)</h4>
                                                </div>
                                                <div class="carrito--item__captionBox">
                                                    <div class="amount-buy">
                                                        <div class="number">
                                                            <span class="minus">-</span>
                                                            <input type="text" value="1"/>
                                                            <span class="plus">+</span>
                                                        </div>
                                                    </div>
                                                    <span class="precio color-primary">S/ 50.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carrito--costo">
                                            <div class="carrito--costo__text text-right">
                                                <p class="subtotal">Subtotal</p>
                                                <p class="total">Total</p>
                                            </div>
                                            <div class="carrito--costo__precio text-right">
                                                <p class="subtotal">S/ 110.00</p>
                                                <p class="total color-primary">S/ 110.00</p>
                                            </div>
                                            <a href="carrito-de-compras.php" class="carrito--pagar btn btn-red text-may bg-primary">Pagar</a>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                    <!-- End Productos -->
                </div>
            </div>
        </section>
        <!-- End Categorías de Productos -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <script type="text/javascript">
        /* === Categorías - Carrito Fixed === */
        if ($(window).width() > 768) {
            $(window).scroll(function () {
                if ($(window).scrollTop() > 133) { // 133 = Height del Header
                    $('#sticky-carrito').css('position', 'fixed');
                    $('#sticky-carrito').css('top', '153px'); // Espacio de separación entre el header y el sticky
                }

                else if ($(window).scrollTop() <= 289) { // 289 = Height del Footer
                    $('#sticky-carrito').css('position', '');
                    $('#sticky-carrito').css('top', '');
                }
                if ($('#sticky-carrito').offset().top + $("#sticky-carrito").height() > $("#footer").offset().top) {
                    $('#sticky-carrito').css('top', -($("#sticky-carrito").offset().top + $("#sticky-carrito").height() - $("#footer").offset().top));
                }
            });
        }
    </script>
    <!-- End Scripts -->

</body>
</html>
