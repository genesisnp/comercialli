<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <!-- Start Recuperar Contraseña -->
        <section class="section-otros recuperar-contrasena">
            <h3 class="h3 text-center text-may color-primary">¿Haz olvidado tu contraseña?</h3>
            <div class="container-custom c-425">
                <p class="text-center text-otros color-internas">No te preocupes, ingresa tu correo electronico y te enviaremos una contraseña en unos minutos.</p>
                <div class="box-form">
                    <form class="form" action="#" method="post">
                        <div class="form__wrapper">
                            <input type="email" class="form__input bg-input" id="email" name="email">
                            <label class="form__label">
                                <span class="form__label-content">Correo</span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-login btn-red text-may" name="button">Ingresar</button>
                    </form>
                </div>
            </div>
        </section>
        <!-- End Recuperar Contraseña -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
