<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-sucursales">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/nosotros.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-sucursales color-white"></i>
                <h2 class="ttl-banner color-white titles-big">sucursales</h2>
            </div>
        </section>
        <section class="our-shops" id="our-shops">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="row">
                            <div class="container">
                                <ul class="accordion">
                                    <li><a href="#mercaderes" class="accordion-heading titles-big text-uppercase active">mercaderes <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Calle Mercaderes 343 Urb. Las Gardenias - Surco</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong><a href="tel:016181070" class="color-internas">618-1070 / </a> 
                                                                <a href="tel:933672384" class="color-internas">933-672-384</a></strong>
                                                            
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">mercaderes@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:00 - 21:00</li>
                                                                <li class="p-internas">Sab 08:00 - 21:00</li>
                                                                <li class="p-internas">Dom 10:00 - 18:00</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2758.2270754344854!2d-76.98442463175367!3d-12.131154347615917!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac14e3447f30bdc!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583941115147!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#higuereta" class="accordion-heading titles-big text-uppercase">higuereta <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av. Benavides 3582 Urb. Higuereta - Surco</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong><a href="tel:016181072" class="color-internas">618-1072 / </a> 
                                                                <a href="tel:933672500" class="color-internas">933-672-500</a></strong>
                                                            
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">higuereta@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:00 - 20:00</li>
                                                                <li class="p-internas">Sab 09:00 - 18:00</li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4638.795390648792!2d-76.99986427115088!3d-12.12949528669611!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6502eeef2f64222b!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583941558782!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#ate" class="accordion-heading titles-big text-uppercase">ate <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av. Simón Bolívar 114-124, Puruchuco</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong><a href="tel:016181083" class="color-internas">618-1083 / </a> 
                                                                <a href="tel:952547106" class="color-internas">952-547-106</a></strong>
                                                            
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">ate@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:00 - 18:00</li>
                                                                <li class="p-internas">Sab 09:00 - 17:00</li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            
                                            <div class="content-map visible-xs visible-sm">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7804.032565852213!2d-76.93789786442233!3d-12.042399832959916!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6daab67b1a397104!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583941649809!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#pharmax-salaverry" class="accordion-heading titles-big text-uppercase">pharmax salaverry <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av. Salaverry 3100 (CC.Pharmax) - Magdalena</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong><a href="tel:016181080" class="color-internas">618-1080 / </a> 
                                                                <a href="tel:933820235" class="color-internas">933-820-235</a></strong>
                                                            
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">salaverry@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Dom 7:30 - 23:00 (Incluye Feriados)</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3901.2085063130844!2d-77.05623656313945!3d-12.097873679711075!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x56d632722226a38!2sPharmax!5e0!3m2!1ses-419!2spe!4v1583941881777!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#pharmax-encalada" class="accordion-heading titles-big text-uppercase">pharmax encalada <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av. Encalada 1541 (CC.Pharmax) - Surco</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong><a href="tel:933899951" class="color-internas">933-899-951</a></strong>
                                                            
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">pharmaxencalada@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Dom 7:00 - 23:00 (Incluye Feriados)</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7802.324899858005!2d-76.97241251006469!3d-12.101029040395394!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d9b8a7b31b4bbe4!2sPharmax!5e0!3m2!1ses-419!2spe!4v1583941981830!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#san-isidro" class="accordion-heading titles-big text-uppercase">san isidro<i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Calle Andrés Reyes 540 - San Isidro</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong>
                                                                <a href="tel:016181082" class="color-internas">618-1082</a>
                                                                <a href="tel:933672449" class="color-internas">933-672-449</a>
                                                            </strong>
                                                            
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">andresreyes540@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:00 - 20:00</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1640.275906970503!2d-77.02656378239396!3d-12.09464927385027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c865ca2b7009%3A0x72dd14a719ce31e0!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942075206!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#miraflores" class="accordion-heading titles-big text-uppercase">miraflores <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av. Cmdte Espinar 841 - Miraflores</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong>
                                                                <a href="tel:016181086" class="color-internas">618-1086</a>
                                                                <a href="tel:933672469" class="color-internas">933-672-469</a>
                                                            </strong>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">miraflores@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:00 - 20:00</li>
                                                                <li class="p-internas">Sab 9:00 - 16:00</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2758.4274749449964!2d-77.03837038193888!3d-12.111772586355695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c83ee4d633f3%3A0x1acd6c7247293e23!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942151187!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#san-borja" class="accordion-heading titles-big text-uppercase">san borja <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av.Aviación 3484 - San Borja</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong>
                                                                <a href="tel:016181084" class="color-internas">618-1084</a>
                                                                <a href="tel:933672489" class="color-internas">933-672-489</a>
                                                            </strong>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">sanborja@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:00 - 22:00</li>
                                                                <li class="p-internas">Sab 9:00 - 20:00</li>
                                                                <li class="p-internas">Sab 9:00 - 17:00</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1379.238943163133!2d-77.00155140336288!3d-12.106892207528189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7e7854f316b%3A0xaa28eae3755d57ed!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942330045!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#mega-plaza" class="accordion-heading titles-big text-uppercase">mega plaza <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Av. Alfredo Mendiola 3698 CC.Mega Plaza tda.L501A - Independencia</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong>
                                                                <a href="tel:016181090" class="color-internas">618-1090</a>
                                                                <a href="tel:933672421" class="color-internas">933-672-421</a>
                                                            </strong>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">megaplaza@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Dom 9:00 - 22:00 (Incluye Feriados)</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3902.7072571507565!2d-77.0608419330658!3d-11.994746788081876!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9d64f6794789376b!2sMega%20Plaza!5e0!3m2!1ses-419!2spe!4v1583942421933!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#ventas" class="accordion-heading titles-big text-uppercase">ventas corporativas <i class="flecha-accord icon-arrow-left"></i></a>
                                        <div class="accordion-content">
                                            <div class="info-general-shops d-flex">
                                                <div class="content-img-shops hidden-xs hidden-sm">
                                                    <img class="img-cover" src="assets/images/sedes/sede.jpg" alt="">
                                                </div>
                                                <div class="info-sucursal">
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-ubication"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Calle Mercaderes 323 Urb. Las Gardenias - Surco</p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-phone"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas">
                                                            <strong>
                                                                <a href="tel:016181111" class="color-internas">618-1111</a>
                                                            </strong>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-email"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p href="#" class="p-internas"><a href="#" class="color-primary">cotizaciones@comercial-li.com</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="info-shops d-flex">
                                                        <div class="content-icon">
                                                            <i class="icon-acc-int icon-calendar"></i>
                                                        </div>
                                                        <div class="content-text-shops">
                                                            <p class="p-internas">Horario de atención:</p>
                                                            <ul>
                                                                <li class="p-internas">Lun-Vie 8:30 - 18:45</li>
                                                                <li class="p-internas">Sáb 9:00 - 13:00</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- mapa mobile -->
                                            <div class="content-map visible-xs visible-sm">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4638.775456429475!2d-76.98443746192503!3d-12.130640851733201!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac14e3447f30bdc!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942482293!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 hidden-xs hidden-sm">
                        <div class="row">
                            <div class="wrapper-map-desktop">
                                <div class="content-map show" id="mercaderes">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2758.2270754344854!2d-76.98442463175367!3d-12.131154347615917!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac14e3447f30bdc!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583941115147!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="higuereta">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4638.795390648792!2d-76.99986427115088!3d-12.12949528669611!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6502eeef2f64222b!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583941558782!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="ate">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7804.032565852213!2d-76.93789786442233!3d-12.042399832959916!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6daab67b1a397104!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583941649809!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="pharmax-salaverry">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3901.2085063130844!2d-77.05623656313945!3d-12.097873679711075!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x56d632722226a38!2sPharmax!5e0!3m2!1ses-419!2spe!4v1583941881777!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="pharmax-encalada">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7802.324899858005!2d-76.97241251006469!3d-12.101029040395394!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d9b8a7b31b4bbe4!2sPharmax!5e0!3m2!1ses-419!2spe!4v1583941981830!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="san-isidro">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1640.275906970503!2d-77.02656378239396!3d-12.09464927385027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c865ca2b7009%3A0x72dd14a719ce31e0!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942075206!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="miraflores">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2758.4274749449964!2d-77.03837038193888!3d-12.111772586355695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c83ee4d633f3%3A0x1acd6c7247293e23!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942151187!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="san-borja">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1379.238943163133!2d-77.00155140336288!3d-12.106892207528189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7e7854f316b%3A0xaa28eae3755d57ed!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942330045!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="mega-plaza">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3902.7072571507565!2d-77.0608419330658!3d-11.994746788081876!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9d64f6794789376b!2sMega%20Plaza!5e0!3m2!1ses-419!2spe!4v1583942421933!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <div class="content-map" id="ventas">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4638.775456429475!2d-76.98443746192503!3d-12.130640851733201!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac14e3447f30bdc!2sComercial%20Li!5e0!3m2!1ses-419!2spe!4v1583942482293!5m2!1ses-419!2spe" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <script>
        $('.accordion li a').click(function (e) {
            var href = $(this).attr('href');
            $('.accordion li a[href="' + href + '"]').closest('li').addClass('active');

            $('.wrapper-map-desktop .content-map').removeClass('show');
            $('.wrapper-map-desktop .content-map' + href).addClass('show');
        });
    </script>
    <!-- End Scripts -->

</body>
</html>
