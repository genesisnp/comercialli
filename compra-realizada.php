<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>  
        <!-- Start Compra Realizada -->
        <section class="section compraRealizada">
            <div class="container-custom c-992">
                <h3 class="h3 text-center text-may color-primary">Gracias, Juan Perez</h3>
                <p class="text-center text-otros color-internas">Tu compra se realizó con éxito.</p>
                <p class="text-center text-otros color-internas">A continuación te enviaremos un correo de confirmación con el detalle de tu compra.</p>
                <h4 class="h4-otros text-center text-may color-primary">Orden de compra: 654321</h4>
                <div class="tableCarrito">
                    <div class="tableCarrito-head">
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image text-center">
                                <span class="text-may">Imagen</span>
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <span class="text-may">Producto</span>
                                </div>
                                <div class="cantidad text-center">
                                    <span class="text-may">Cantidad</span>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="text-may">Precio Unitario</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="text-may">Subtotal</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tableCarrito-body">
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image">
                                <img class="img-responsive" src="assets/images/carrito/producto-02.jpg" alt="Producto">
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <p class="color-internas">Parlantes Xtreme Usb (Kes-215A)</p>
                                    <span class="color-primary">Código: 137295</span>
                                </div>
                                <div class="cantidad">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="2"/>
                                            <span class="plus">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="color-internas">S/ 69.00</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="color-internas">S/ 138.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image">
                                <img class="img-responsive" src="assets/images/carrito/producto-04.jpg" alt="Producto">
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <p class="color-internas">Tomatodo para batidos</p>
                                    <span class="color-primary">Código: 137295</span>
                                </div>
                                <div class="cantidad">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="2"/>
                                            <span class="plus">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="color-internas">S/ 7.00</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="color-internas">S/ 14.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="tableCarrito--row">
                            <div class="tableCarrito--row__image">
                                <img class="img-responsive" src="assets/images/carrito/producto-05.jpg" alt="Producto">
                            </div>
                            <div class="tableCarrito--row__caption">
                                <div class="producto">
                                    <p class="color-internas">Boys Pique Polo</p>
                                    <span class="color-primary">Código: 137295</span>
                                </div>
                                <div class="cantidad">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="3"/>
                                            <span class="plus">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="precioUnitario text-center">
                                    <span class="color-internas">S/ 25.00</span>
                                </div>
                                <div class="subtotal text-center">
                                    <span class="color-internas">S/ 150.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid compraRealizada--grid">
                    <div class="col-3">
                        <p class="text-otros color-internas"><span class="titles-int">Fecha:</span> 04/03/2020</p>
                        <p class="p-internas color-primary"><span class="titles-int">Estimado(a) Juan Perez</span></p>
                        <p class="text-otros color-internas"><span class="titles-int">Lugar de envio:</span> Av. Precursores 784 - San Miguel</p>
                        <p class="text-otros color-internas"><span class="titles-int">Entrega:</span> 07/03/2020</p>
                        <p class="text-otros color-internas"><span class="titles-int">Responsable de recibir:</span> Lucia</p>
                        <p class="text-otros color-internas"><span class="titles-int">Hora:</span> 08:00 - 10:00</p>
                    </div>
                    <div class="col-3">
                        <p class="text-otros color-internas"><span class="titles-int">Número de tarjeta:</span> 321************654</p>
                        <p class="p-internas color-primary"><span class="titles-int">RUC: 109876532546</span></p>
                    </div>
                    <div class="col-3">
                        <div class="grid">
                            <div class="col-2">
                                <p class="titles-int color-internas">Sub total:</p>
                                <p class="titles-int color-internas">Delivery:</p>
                                <p class="titles-int color-primary">Total:</p>
                            </div>
                            <div class="col-2">
                                <p class="titles-int color-internas">s/ 150.00</p>
                                <p class="titles-int color-internas">Gratis</p>
                                <p class="titles-int color-primary">s/ 150.00</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="index.php" class="btn btn-red">Seguir comprando</a>
                </div>
            </div>
        </section>
        <!-- End Compra Realizada -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
