
/* === Animated.css - Wow.js === */
var wow = new WOW(
    {
        boxClass: 'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0,          // distance to the element when triggering the animation (default is 0)
        mobile: false,       // trigger animations on mobile devices (default is true)
        live: true,       // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();

/* === Back === */
function goBack() {
	window.history.back();
}

$(document).ready(function(){
    /* === Header - Fixed === */
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= 10) {
            $('.header').addClass('header-fixed');
            $('main').addClass('main-headerFixed');
        }
        else {
            $('.header').removeClass('header-fixed');
            $('main').removeClass('main-headerFixed');
        }
    });

    /* === Header - Main Menu === */
    $('.openClose-mainMenu').click(function () {
        $('body').toggleClass('menu-open');
        //$('.header').toggleClass('z-header');
    });

    /* === Header - Menu Otros === */
    $('.servicios-link').click(function () {
        $('body').toggleClass('serviciosCliente-open');
    });

    /* === Header - Menu Otros === */
    $('.otros-link').click(function () {
        $('body').toggleClass('otros-open');
    });

    /* === Header - Hamburger Menu === */
    $('.hamburger-menu').click(function () {
        $(this).toggleClass('open');
    });

    /* === Add Background Opacity ===
    if ($(window).width() >= 768) {
        $('.navbar--categorias').click(function () {
            if ($('body').hasClass('bg-opacity')) {
                $('body').removeClass('bg-opacity');
            } else {
                $('body').addClass('bg-opacity');
            }
        });
    }*/

    /* === Remove Background Opacity === 
    $(document).click(function () {
        $('#main-menu').removeClass('menu-open');
        $('body').removeClass('bg-opacity');
    });*/

    /* Cuando da click en el link categoría el contenido de subcategorias se ocultará
    $('.categoria__heading a').click(function () {
        $('.subcategorias').css({
            'opacity': '0'
        });
    });*/

    /* === Header - Carrito === */
    if ($(window).width() >= 768) {
        $("#shopping-cart").mouseenter(function () {
            $('#carrito-header').fadeIn();
        });
        $("#shopping-cart").mouseleave(function () {
            $('#carrito-header').fadeOut();
        });
        $('#cerrar-carrito').click(function () {
            $('#carrito-header').fadeOut();
        });
    }
    
    /* === Header - Submenu ===  */
    $('#menu-servicios').click(function () {
        $('.navbar--menu .subnavbar').slideToggle();
    });

    /* === Header - Main Menu Responsive === */
    $('#menu-responsive').click(function () {
        $('body').toggleClass('menu-open');
    });

    /* === Header - Menu subcategorías === */
    if ($(window).width() <= 768) {
        $('.subcategorias').addClass('collapse');
    }
    /*if ($(window).width() <= 768) {
        $('.categoria__heading a').click(function () {
            $('.subcategorias').toggleClass('submenu-show');
        });
    }*/

    /* === Home Slider === */
    $('.slider').slick({
        arrows: true,
        dots: true,
        infinite: false,
        nextArrow:'<button class="slick-next"> <span class="icon-arrow-right"></span> </button>',
		prevArrow:'<button class="slick-prev"> <span class="icon-arrow-left"></span> </button>',
        speed: 500,
        cssEase: 'linear',
        fade:true,
        draggable: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    /* === Slider Ofertas === */
    $('.slider-ofertas').slick({
        arrows: true,
        dots: true,
        infinite: false,
        nextArrow:'<button class="slick-next"> <span class="icon-arrow-right"></span> </button>',
		prevArrow:'<button class="slick-prev"> <span class="icon-arrow-left"></span> </button>',
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    dots: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    /* === Slider Productos Destacados === */
    $('.slider-productosDestacados').slick({
        arrows: true,
        dots: true,
        infinite: false,
        nextArrow:'<button class="slick-next"> <span class="icon-arrow-right"></span> </button>',
		prevArrow:'<button class="slick-prev"> <span class="icon-arrow-left"></span> </button>',
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    dots: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    /* === Tabs Productos Destacados === */
    $('.tabs .tab-item').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.tabs .tab-item').removeClass('current');
        $('.tab-panel').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

    /* === Amount Product === */
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    /* === Hover Servicios === */
    if ($(window).width() >= 768) {
        $('.home-servicio--item').hover(function () {
            $(this).find('.caption').stop().animate({
                height: "toggle",
                opacity: "toggle"
            }, 300);
        });
    }

    /* === Slider Home Servicios === */
    $('.slider-homeServicios').slick({
        arrows: true,
        dots: false,
        infinite: false,
        nextArrow:'<button class="slick-next"> <span class="icon-arrow-right"></span> </button>',
		prevArrow:'<button class="slick-prev"> <span class="icon-arrow-left"></span> </button>',
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    /* === Form Input Custom === 
    $('input').on('focusin', function() {
        $(this).parent().find('label').addClass('activeLabel');
    });
    
    $('input').on('focusout', function() {
        if (!this.value) {
            $(this).parent().find('label').removeClass('activeLabel');
        }
    });*/

    /* === Cotizar - Hide === */
    $('#cerrar-cotizar').click(function(){
        $('#cotizar').css("transform", "translateY(584px)");
    });

    /* === Slider Productos Relacionados === */
    $('.slider-productosRelacionados').slick({
        arrows: true,
        dots: true,
        infinite: false,
        nextArrow:'<button class="slick-next"> <span class="icon-arrow-right"></span> </button>',
		prevArrow:'<button class="slick-prev"> <span class="icon-arrow-left"></span> </button>',
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    dots: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    /* === Footer - Link Servicio al cliente === */
    $('.footer-col:first-child h5').on('click', function () {
        $('.footer-col:first-child .footer-list').slideToggle();
        //$('.footer-col:first-child h5 i').toggleClass('active');
    });

    /* === xZoom === */
	$('.xzoom, .xzoom__thumb').xzoom({
		position: 'inside',
		tint: '#333',
		//title: true,
		//titleClass: 'xzoom-caption',
		zoomWidth: 300,
		zoomHeight: 300
    });

    
    
    /* === Accordion === */
    (function ($) {
        $(".accordion > li:eq(0) a")
            .addClass("active")
            .next()
            .slideDown();

        $(".accordion a").click(function (j) {
            var dropDown = $(this)
                .closest("li")
                .find(".accordion-content");

            $(this)
                .closest(".accordion")
                .find(".accordion-content")
                .not(dropDown)
                .slideUp();

            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            } else {
                $(this)
                    .closest(".accordion")
                    .find("a.active")
                    .removeClass("active");
                $(this).addClass("active");
            }

            dropDown.stop(false, true).slideToggle();

            j.preventDefault();
        });
    })(jQuery);

    /* === Slider Resp. Social === */
    $('.slider-resp-social').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        nextArrow: '<button class="slick-next"> <span class="icon-arrow-right"></span> </button>',
        prevArrow: '<button class="slick-prev"> <span class="icon-arrow-left"></span> </button>',
    });

    /* === Categorías Toggle === */
    $(function () {
        $('.categorias--heading').on('click', function () {
            $('.categorias--body').slideToggle();
        });
    });

    /* === Plus Minus Toggle === */
    $(function () {
        $('.categorias--heading').on('click', function () {
            $('.plus-minus-toggle').toggleClass('collapsed');
        });
    });

    /* === Form - input === */
    $('.form__input').blur(function () {
        if ($(this).val()) {
            $(this).closest('.form__wrapper').addClass('form--filled');
        } else {
            $(this).closest('.form__wrapper').removeClass('form--filled');
        }
    });

    /* === Form - file === */
    $("#file").on("change", function (e) {
        var files = $(this)[0].files;
        if (files.length >= 2) {
            $(".file_label").text(files.length + " Adjuntar archivo");
        } else {
            var fileName = e.target.value.split("\\").pop();
            $(".file_label").text(fileName);
        }
    });

    /* === datepicker - Carrito de compras === */
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function () {
        $("#datepicker-carrito").datepicker({
            //  execute a function whenever the user changes the view
            //  (days/months/years), as well as when the user
            //  navigates by clicking on the "next"/"previous" icons
            //  in any of the views
            always_visible: $('#datepicker-carrito'),
            prevText: '<i class="icon-arrow-left"></i>',
            nextText: '<i class="icon-arrow-right"></i>',
            selectOtherMonths: true,
            showOtherMonths: true
        });
    });

    /* DETALLE PRODUCTO - Comprar Fixed */
    if ($(window).width() <= 576) {
        $("#comprarFixed").appendTo(".detalleProducto-info--body");
    }

    /* DETALLE PRODUCTO - BTN Collapse */
    $('#btn-info').click(function () {
        $('.detalleProducto-info--body .infoProducto').slideToggle();
        $('.detalleProducto-info--body .detalleProducto--form').slideUp();
    });
    $('#btn-esp').click(function () {
        $('.detalleProducto-info--body .detalleProducto--form').slideToggle();
        $('.detalleProducto-info--body .infoProducto').slideUp();
    });

    /* === Servicios - Links active with tabs ===
    //grabs the hash tag from the url
    var hash = window.location.hash;
    //checks whether or not the hash tag is set
    if (hash != "") {
        //removes all active classes from tabs
        $('.main-sucursales .nav-tabs li').each(function () {
            $(this).removeClass('active');
        });
        $('.main-sucursales .tab-content div').each(function () {
            $(this).removeClass('in active',);
        });
        //this will add the active class on the hashtagged value
        var link = "";
        $('.main-sucursales .nav-tabs li').each(function () {
            link = $(this).find('a').attr('href');
            if (link == hash) {
                $(this).addClass('active');
            }
        });
        $('.main-sucursales .tab-content div').each(function () {
            link = $(this).attr('id');
            if ('#' + link == hash) {
                $(this).addClass('in active',);
            }
        });
    } */
    
});

