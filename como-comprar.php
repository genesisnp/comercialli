<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-fcomo-comprar">
        <section class="banner mg-b-banner">
            <img class="img-cover" src="assets/images/banner/como-comprar.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-como-comprar color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Como comprar</h2>
            </div>
        </section>
        <section class="como-comprar">
            <div class="container flex-mob">
                <div class="row d-flex-just">
                    <div class="card-como-comprar col-xs-12 col-md-4 wow zoomIn" data-wow-delay="1s">
                        <div class="row d-flex-just">
                            <div class="col-xs-11 col-sm-8">
                                <div class="content-card-repeat text-center">
                                    <div class="content-icon-card">
                                        <i class="icon-tabs icon-select-product"></i>
                                    </div>
                                    <h2 class="ttl-tab-op1 text-uppercase titles-big color-internas">seleccionar<br>el producto</h2>
                                    <p class="p-internas">Ingrese a la opción PRODUCTOS, elige los productos que desees y agrégalos a tu carrito de compras.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-como-comprar col-xs-12 col-md-4 wow zoomIn" data-wow-delay="2s">
                        <div class="row d-flex-just">
                            <div class="col-xs-11 col-sm-8">
                                <div class="content-card-repeat text-center">
                                    <div class="content-icon-card">
                                        <i class="icon-tabs icon-compl-form"></i>
                                    </div>
                                    <h2 class="ttl-tab-op1 text-uppercase titles-big color-internas">complete<br>el formulario</h2>
                                    <p class="p-internas">Completa todos los campos que parecen en el formulario y presiona la opción pagar.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-como-comprar col-xs-12 col-md-4 wow zoomIn" data-wow-delay="3s">
                        <div class="row d-flex-just">
                            <div class="col-xs-11 col-sm-8">
                                <div class="content-card-repeat text-center">
                                    <div class="content-icon-card">
                                        <i class="icon-tabs icon-met-pago"></i>
                                    </div>
                                    <h2 class="ttl-tab-op1 text-uppercase titles-big color-internas">seleccione<br>el método de pago</h2>
                                    <p class="p-internas">Completa el sistema de pago con tu tarjeta de preferencia.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>

