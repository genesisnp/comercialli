<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-terms-conditions">
        <section class="banner mg-b-banner">
            <img class="img-cover" src="assets/images/banner/termin-y-cond.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-cambios color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Términos y condiciones</h2>
            </div>
        </section>
        <section class="sct-acc-terms">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 float-center">
                        <ul class="accordion">
                            <li><a class="accordion-heading titles-int text-uppercase">atención de pedidos <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto quos obcaecati rerum! Laborum a asperiores modi quos obcaecati 
                                        rerum! Laborum a asperiores modi omnis quos obcaecati rerum! Laborum a asperiores modipraesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">Servicio delivery <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">confirmación de pago <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">fechas especiales <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">emisión de facturas y boletas <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">devoluciones <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">anulacion de pedidos <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">stock de pedidos <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">descuentos <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int text-uppercase">importante <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
