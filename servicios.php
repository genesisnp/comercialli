<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <main class="main-sucursales">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-nuestros-servicios color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Nuestros servicios</h2>
            </div>
        </section>

        <section class="sct-tabs-services">
            <div class="content-navb-tab bg-forma">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs row" role="tablist">
                                <li role="presentation" class="item-tabs col-xs-2 active wow zoomIn">
                                    <a href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab">
                                        <div class="content-card-repeat">
                                            <div class="content-icon-card">
                                                <i class="icon-tabs icon-delivery"></i>
                                            </div>
                                            <h2 class="ttl-tab text-uppercase titles-big color-internas">delivery</h2>
                                        </div>
                                    </a>
                                </li>
                                <li role="presentation" class="item-tabs col-xs-2 wow zoomIn" data-wow-delay="0.5s">
                                    <a href="#credito-hip" aria-controls="credito-hip" role="tab" data-toggle="tab">
                                        <div class="content-card-repeat">
                                            <div class="content-icon-card">
                                                <i class="icon-tabs icon-credito-coorporativo"></i>
                                            </div>
                                            <h2 class="ttl-tab text-uppercase titles-big color-internas">crédito<br>corporativo</h2>
                                        </div>
                                    </a>
                                </li>
                                <li role="presentation" class="item-tabs col-xs-2 wow zoomIn" data-wow-delay="1s">
                                    <a href="#ped-por-cont" aria-controls="ped-por-cont" role="tab" data-toggle="tab">
                                        <div class="content-card-repeat">
                                            <div class="content-icon-card">
                                                <i class="icon-tabs icon-pedidos"></i>
                                            </div>
                                            <h2 class="ttl-tab text-uppercase titles-big color-internas">pedidos<br>con contrato</h2>
                                        </div>
                                    </a>
                                </li>
                                <li role="presentation" class="item-tabs col-xs-2 wow zoomIn" data-wow-delay="1.5s">
                                    <a href="#vales-cons" aria-controls="vales-cons" role="tab" data-toggle="tab">
                                        <div class="content-card-repeat">
                                            <div class="content-icon-card">
                                                <i class="icon-tabs icon-vales"></i>
                                            </div>
                                            <h2 class="ttl-tab text-uppercase titles-big color-internas">vales de<br>consumo</h2>
                                        </div>
                                    </a>
                                </li>
                                <li role="presentation" class="item-tabs col-xs-2 wow zoomIn" data-wow-delay="2s">
                                    <a href="#serv-pers" aria-controls="serv-pers" role="tab" data-toggle="tab">
                                        <div class="content-card-repeat">
                                            <div class="content-icon-card">
                                                <i class="icon-tabs icon-serv-personalizado"></i>
                                            </div>
                                            <h2 class="ttl-tab text-uppercase titles-big color-internas">servicio<br>personalizado</h2>
                                        </div>
                                    </a>
                                </li>
                                <li role="presentation" class="item-tabs col-xs-2 wow zoomIn" data-wow-delay="2.5s">
                                    <a href="#at-colegios" aria-controls="at-colegios" role="tab" data-toggle="tab">
                                        <div class="content-card-repeat">
                                            <div class="content-icon-card">
                                                <i class="icon-tabs icon-atenc-cole"></i>
                                            </div>
                                            <h2 class="ttl-tab text-uppercase titles-big color-internas">atención<br>a colegios</h2>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="delivery">
                            <img class="tabpanel--image img-responsive" src="assets/images/internas/delivery.png" alt="Delivery">
                            <div class="tabpanel--caption captionRight">
                                <i class="icon-delivery"></i>
                                <h3 class="text-may color-internas">Delivery</h3>
                                <p class="p-internas">¡Gratuito y puntual! Contamos con el mejor servicio de delivery del mercado en puntualidad y calidad.</p>
                                <p class="p-internas">Para pedidos sobre S/ 1000.00 el delivery es gratuito dentro de Lima Metropolitana.</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="credito-hip">
                            <div class="tabpanel--caption captionLeft">
                                <i class="icon-credito-coorporativo"></i>
                                <h3 class="text-may color-internas">Crédito Corporativo</h3>
                                <ul>
                                    <li>
                                        <h4 class="h4 color-internas">Crédito Corporativo</h4>
                                        <p class="p-internas">Sin recargos ni intereses, ofrecemos a empresas u organizaciones la posibilidad de pagar en distintos plazos y modalidades.</p>
                                    </li>
                                    <li>
                                        <h4 class="h4 color-internas">Crédito Corporativo Escolar</h4>
                                        <p class="p-internas">¡Para que en marzo no se queden con los bolsillos vacíos! Por campaña escolar, ofrecemos crédito en útiles escolares a los colaboradores de tu empresa u organización. Paga en cuotas a partir de abril.</p>
                                    </li>
                                </ul>
                            </div>
                            <img class="tabpanel--image imageRight img-responsive fadeInRight" src="assets/images/internas/credito-corporativo.png" alt="Crédito Corporativo">
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="ped-por-cont">
                            <img class="tabpanel--image img-responsive" src="assets/images/internas/pedidos-por-contrato.png" alt="Pedidos con contrato">
                            <div class="tabpanel--caption captionRight">
                                <i class="icon-pedidos"></i>
                                <h3 class="text-may color-internas">Pedidos con contrato</h3>
                                <p class="p-internas">¡Gratuito y puntual! Contamos con el mejor servicio de delivery del mercado en puntualidad y calidad. Para pedidos sobre S/ 1000.00 el delivery es gratuito dentro de Lima Metropolitana.</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="vales-cons">
                            <div class="tabpanel--caption captionLeft">
                                <i class="icon-vales"></i>
                                <h3 class="text-may color-internas">Vales de Consumo</h3>
                                <p class="p-internas">Para premiar a los colaboradores de tu empresa, ofrecemos vales de compra para que los hagan válidos en nuestros puntos de venta.</p>
                            </div>
                            <img class="tabpanel--image imageRight img-responsive" src="assets/images/internas/vales-consumo.png" alt="Vales de Consumo">
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="serv-pers">
                            <img class="tabpanel--image img-responsive" src="assets/images/internas/servicio-personalizado.png" alt="Servicio Personalizado">
                            <div class="tabpanel--caption captionRight">
                                <i class="icon-serv-personalizado"></i>
                                <h3 class="text-may color-internas">Servicio Personalizado</h3>
                                <p class="p-internas">Para brindarte un servicio integral, atendemos pedidos de acuerdo a las necesidades administrativas de tu institución sin costo adicional. Entre ellos están la atención y facturación por centro de costos y la elaboración de reportes.</p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="at-colegios">
                            <div class="tabpanel--caption captionLeft">
                                <i class="icon-atenc-cole"></i>
                                <h3 class="text-may color-internas">Atención a Colegios</h3>
                                <p class="p-internas">Ofrecemos servicios y productos exclusivos a distintos colegios. <br>Si eres un colegio y deseas más información llámanos al <b>618 1111</b></p>
                            </div>
                            <img class="tabpanel--image imageRight img-responsive" src="assets/images/internas/atencion-colegios.png" alt="Atención a Colegios">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
