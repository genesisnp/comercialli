<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>  
        <!-- Start Búsqueda Fallida -->
        <section class="section-slider busquedaFallida">
            <div class="container">
                <p class="text-otros text-center color-internas">Lo sentimos, no pudimos encontrar tu producto.</p>
                <!-- Start Productos Relacionados -->
                <h3 class="h3 color-primary text-center">Productos Relacionados</h3>
                <div class="slider-productosRelacionados slider--arrows slider--dots">
                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos Con Memoria Interna <br> NW-WS413 Verde</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 349.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <!--
                                    <i class="icon-carrito color-white carritoHover"></i>
                                    -->
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlante Flip 4 Acuático</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 399.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlantes Inalámbricos SoundLink <br> Color II Rojo</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 599.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos In-Ear Con Micrófono <br> MDR-EX15AP Morado</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 38.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos Con Memoria Interna <br> NW-WS413 Verde</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 349.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <!--
                                    <i class="icon-carrito color-white carritoHover"></i>
                                    -->
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlante Flip 4 Acuático</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 399.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlantes Inalámbricos SoundLink <br> Color II Rojo</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 599.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos In-Ear Con Micrófono <br> MDR-EX15AP Morado</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 38.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Productos Relacionados -->
            </div>
        </section>
        <!-- End Búsqueda Fallida -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
