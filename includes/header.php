<header class="header">
    <div class="bg-primary">
        <div class="container">
            <div class="header--wrapper">
                <div class="head">
                    <div class="head--left">
                        <div id="menu-responsive" class="hamburger-menu">
                            <span class="icon"></span>
                        </div>
                        <div class="social">
                            <a href=""><i class="icon-facebook color-white"></i></a>
                            <a href=""><i class="icon-twitter color-white"></i></a>
                            <a href=""><i class="icon-instagram color-white"></i></a>
                            <a href=""><i class="icon-youtube color-white"></i></a>
                            <a href=""><i class="icon-linkedin color-white"></i></a>
                        </div>
                        <div class="phone">
                            <a href="tel:6181111">
                                <i class="icon-phone color-white"></i>
                                <span class="color-white">618 1111</span>
                            </a>
                        </div>
                        <div class="email">
                            <a href="mailto:cotizaciones@comercial-li.com" target="_blank">
                                <i class="icon-sobre color-white"></i>
                                <span class="color-white underlineHover">cotizaciones@comercial-li.com</span>
                            </a>
                        </div>
                    </div>
                    <div class="head--center">
                        <h1 class="logo">
                            <a href="index.php">
                                <img class="img-responsive" src="assets/images/logo.png" alt="logo">
                            </a>
                        </h1>
                    </div>
                    <div class="head--right">
                        <div class="catalogo">
                            <a href="" class="catalogo--item color-white text-may">Catálogo</a>
                        </div>
                        <div class="login">
                            <a href="iniciar-sesion.php"><i class="icon-login color-white"></i></a>
                        </div>
                        <div id="shopping-cart" class="shopping-cart">
                            <a href="carrito-de-compras.php">
                                <span class="cantidad bg-white color-primary">1</span>
                                <i class="icon-carrito color-white"></i>
                            </a>
                            <div id="carrito-header" class="carrito text-center bg-white">
                                <a id="cerrar-carrito" href="javascript:void(0)" class="cerrar btn-red">
                                    <span class="line line1"></span>
                                    <span class="line line2"></span>
                                </a>
                                <h3 class="color-primary">Tu carrito</h3>
                                <p>(Tienes 3 productos)</p>
                                <div class="carrito--item">
                                    <img class="carrito--item__image img-responsive" src="assets/images/carrito/producto-01.jpg" alt="Imagen Producto Carrito">
                                    <div class="carrito--item__caption">
                                        <div class="carrito--item__captionBox">
                                            <h4 class="color-internas">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                            <a href="javascriptvoid:(0)" class="tachito">
                                                <i class="icon-tachito"></i>
                                            </a>
                                        </div>
                                        <div class="carrito--item__captionBox">
                                            <div class="amount-buy">
                                                <div class="number">
                                                    <span class="minus">-</span>
                                                    <input type="text" value="1"/>
                                                    <span class="plus">+</span>
                                                </div>
                                            </div>
                                            <span class="precio color-primary">S/ 20.00</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="carrito--item">
                                    <img class="carrito--item__image img-responsive" src="assets/images/carrito/producto-02.jpg" alt="Imagen Producto Carrito">
                                    <div class="carrito--item__caption">
                                        <div class="carrito--item__captionBox">
                                            <h4 class="color-internas">Parlantes Xtreme Usb (Kes-215A)</h4>
                                            <a href="javascriptvoid:(0)" class="tachito">
                                                <i class="icon-tachito"></i>
                                            </a>
                                        </div>
                                        <div class="carrito--item__captionBox">
                                            <div class="amount-buy">
                                                <div class="number">
                                                    <span class="minus">-</span>
                                                    <input type="text" value="1"/>
                                                    <span class="plus">+</span>
                                                </div>
                                            </div>
                                            <span class="precio color-primary">S/ 30.00</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="carrito--item">
                                    <img class="carrito--item__image img-responsive" src="assets/images/carrito/producto-03.jpg" alt="Imagen Producto Carrito">
                                    <div class="carrito--item__caption">
                                        <div class="carrito--item__captionBox">
                                            <h4 class="color-internas">Parlantes Xtreme Usb (Kes-215A)</h4>
                                            <a href="javascriptvoid:(0)" class="tachito">
                                                <i class="icon-tachito"></i>
                                            </a>
                                        </div>
                                        <div class="carrito--item__captionBox">
                                            <div class="amount-buy">
                                                <div class="number">
                                                    <span class="minus">-</span>
                                                    <input type="text" value="1"/>
                                                    <span class="plus">+</span>
                                                </div>
                                            </div>
                                            <span class="precio color-primary">S/ 50.00</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="carrito--costo">
                                    <div class="carrito--costo__text text-right">
                                        <p class="subtotal">Subtotal</p>
                                        <p class="descuentos">Descuentos</p>
                                        <p class="total">Total</p>
                                    </div>
                                    <div class="carrito--costo__precio text-right">
                                        <p class="subtotal">S/ 110.00</p>
                                        <p class="descuentos">- S/ 0.00</p>
                                        <p class="total color-primary">S/ 110.00</p>
                                    </div>
                                    <a href="carrito-de-compras.php" class="carrito--pagar btn btn-red text-may bg-primary">Pagar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="navbar">
                    <div class="navbar-left">
                        <button type="button" class="navbar--categorias openClose-mainMenu bg-white color-internas text-may" name="navbar--categorias">
                            <span>Categorías</span>
                            <i class="arrow-down"></i>
                            <i class="icon-arrow-right color-internas"></i>
                        </button>
                        <a href="categorias.php" class="navbar--item color-white text-may">Navidad</a>
                        <a href="categorias.php" class="navbar--item color-white text-may">Novedades</a>
                        <a href="categorias.php" class="navbar--item itemHideMobile color-white text-may">Escritorio</a>
                    </div>
                    <div class="navbar-right">
                        <a href="categorias.php" class="navbar--item itemHideMobile color-white text-may">Oficina</a>
                        <a href="categorias.php" class="navbar--item itemHideMobile color-white text-may">Juguetes</a>
                        <a href="categorias.php" class="navbar--item itemHideMobile color-white text-may">Cómputo y Tecnología</a>
                        <a href="categorias.php" class="navbar--item itemHideDesktop color-white text-may">Ofertas del Mes</a>
                        <a href="categorias.php" class="navbar--item itemHideDesktop color-white text-may">Productos Destacados</a>
                        <div class="navbar--menu">
                            <div id="menu-servicios" class="hamburger-menu">
                                <span class="icon"></span>
                            </div>
                            <nav class="subnavbar">
                                <div class="subnavbar--wrapper">
                                    <div class="submenu">
                                        <div class="submenu-col subnavbar--otros">
                                            <a href="nosotros.php" class="submenu--item">
                                                <i class="icon-logo color-internas"></i>
                                                <h3 class="h3 text-may color-internas">Nosotros</h3>
                                            </a>
                                            <a href="servicios.php" class="submenu--item">
                                                <i class="icon-nuestros-servicios color-internas"></i>
                                                <h3 class="h3 text-may color-internas">Nuestros Servicios</h3>
                                            </a>
                                            <a href="sucursales.php" class="submenu--item">
                                                <i class="icon-sucursales color-internas"></i>
                                                <h3 class="h3 text-may color-internas">Sucursales</h3>
                                            </a>
                                            <a href="contactenos.php" class="submenu--item">
                                                <i class="icon-contacto color-internas"></i>
                                                <h3 class="h3 text-may color-internas">Contáctanos</h3>
                                            </a>
                                        </div>
                                        <div class="submenu-col">
                                            <h3 class="h3 text-may color-internas">Servicio al cliente</h3>
                                            <button type="button" class="btn--volver bg-primary color-white servicios-link text-may" name="volver">
                                                <span>Volver</span>
                                                <i class="icon-arrow-left"></i>
                                            </button>
                                            <ul class="submenu--list">
                                                <li><a href="como-comprar.php" class="submenu--list__item color-internas">Como comprar</a></li>
                                                <li><a href="preguntas-frecuentes.php" class="submenu--list__item color-internas">Preguntas frecuentes</a></li>
                                                <li><a href="formas-de-pago.php" class="submenu--list__item color-internas">Forma de pago</a></li>
                                                <li><a href="politica-de-entrega.php" class="submenu--list__item color-internas">Política de entrega</a></li>
                                                <li><a href="cambios-y-devoluciones.php" class="submenu--list__item color-internas">Cambios y devoluciones</a></li>
                                                <li><a href="terminos-y-condiciones.php" class="submenu--list__item color-internas">Términos y condiciones</a></li>
                                                <li><a href="politicas-de-privacidad.php" class="submenu--list__item color-internas">Política de privacidad</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="navbar-otros">
                        <a href="javascript:void(0)" class="navbar--item servicios-link color-internas text-may">
                            <span>Servicios al Cliente</span>
                            <i class="icon-arrow-right"></i>
                        </a>
                        <a href="javascript:void(0)" class="navbar--item otros-link color-internas text-may">
                            <span>Otros Enlaces</span>
                            <i class="icon-arrow-right"></i>
                        </a>
                    </div>
                    <div class="social">
                        <a href=""><i class="icon-facebook color-internas"></i></a>
                        <a href=""><i class="icon-twitter color-internas"></i></a>
                        <a href=""><i class="icon-instagram color-internas"></i></a>
                        <a href=""><i class="icon-youtube color-internas"></i></a>
                        <a href=""><i class="icon-linkedin color-internas"></i></a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="bg-redDark">
        <form class="form search" action="">
            <div class="form__wrapper">
                <input type="text" class="form__input bg-input" id="name" name="name">
                <label class="form__label">
                    <span class="form__label-content">¿Qué estás buscando?</span>
                </label>
                <button type="submit" class="btn-lupa bg-gray" name="button">
                    <i class="icon-lupa color-white"></i>
                </button>
            </div>
        </form>
    </div>
    <!-- Start Main Menu -->
    <div id="main-menu" class="main-menu bg-white">
        <a href="javascript:void(0)" class="cerrar btn-red openClose-mainMenu">
            <span class="line line1"></span>
            <span class="line line2"></span>
        </a>
        <button type="button" class="navbar--categorias bg-white color-internas text-may" name="navbar--categorias">
            <span>Categorías</span>
            <i class="arrow-down"></i>
        </button>
        <ul class="menu">
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-oficina">
                    <a href="#" class="color-internas">Oficina</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-oficina">
                    <h3 class="h3 color-internas">Oficina</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-computo">
                    <a href="#" class="color-internas">Cómputo y tecnología</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-computo">
                    <h3 class="h3 color-internas">Cómputo y tecnología</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-coffee">
                    <a href="#" class="color-internas">Coffee break</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-coffee">
                    <h3 class="h3 color-internas">Coffee break</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-limpieza">
                    <a href="#" class="color-internas">Limpieza y mantenimiento</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-limpieza">
                    <h3 class="h3 color-internas">Limpieza y mantenimiento</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-escolar">
                    <a href="#" class="color-internas">Escolar</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-escolar">
                    <h3 class="h3 color-internas">Escolar</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-arte">
                    <a href="#" class="color-internas">Arte y Manualidades</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-arte">
                    <h3 class="h3 color-internas">Arte y Manualidades</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-entretenimiento">
                    <a href="#" class="color-internas">Entretenimiento</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-entretenimiento">
                    <h3 class="h3 color-internas">Entretenimiento</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-linea-ejecutiva">
                    <a href="#" class="color-internas">Línea ejecutiva</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-linea-ejecutiva">
                    <h3 class="h3 color-internas">Línea ejecutiva</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-oficina2">
                    <a href="#" class="color-internas">Oficina</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-oficina2">
                    <h3 class="h3 color-internas">Oficina</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-computo2">
                    <a href="#" class="color-internas">Cómputo y tecnología</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-computo2">
                    <h3 class="h3 color-internas">Cómputo y tecnología</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-coffee2">
                    <a href="#" class="color-internas">Coffee break</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-coffee2">
                    <h3 class="h3 color-internas">Coffee break</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-limpieza2">
                    <a href="#" class="color-internas">Limpieza y mantenimiento</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-limpieza2">
                    <h3 class="h3 color-internas">Limpieza y mantenimiento</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-escolar2">
                    <a href="#" class="color-internas">Escolar</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-escolar2">
                    <h3 class="h3 color-internas">Escolar</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-arte2">
                    <a href="#" class="color-internas">Arte y Manualidades</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-arte2">
                    <h3 class="h3 color-internas">Arte y Manualidades</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-entretenimiento2">
                    <a href="#" class="color-internas">Entretenimiento</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-entretenimiento2">
                    <h3 class="h3 color-internas">Entretenimiento</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-linea-ejecutiva2">
                    <a href="#" class="color-internas">Línea ejecutiva</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-linea-ejecutiva2">
                    <h3 class="h3 color-internas">Línea ejecutiva</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-oficina3">
                    <a href="#" class="color-internas">Oficina</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-oficina3">
                    <h3 class="h3 color-internas">Oficina</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-computo3">
                    <a href="#" class="color-internas">Cómputo y tecnología</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-computo3">
                    <h3 class="h3 color-internas">Cómputo y tecnología</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-coffee3">
                    <a href="#" class="color-internas">Coffee break</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-coffee3">
                    <h3 class="h3 color-internas">Coffee break</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="categoria">
                <div class="categoria__heading" data-toggle="collapse" data-target="#sub-limpieza3">
                    <a href="#" class="color-internas">Limpieza y mantenimiento</a>
                    <i class="icon-arrow-right color-white"></i>
                </div>
                <div class="subcategorias" id="sub-limpieza3">
                    <h3 class="h3 color-internas">Limpieza y mantenimiento</h3>
                    <div class="subcategorias--list grid">
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                        <div class="subcategoria col-3">
                            <h4 class="h4 text-may color-primary">Subcategoría</h4>
                            <ul class="subcategoria--list">
                                <li><a href="categorias.php" class="color-internas underlineHover">Adhesivos y pegamentos</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Artículos para fiestas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cerámica y pastas</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Cuadernos y blocks</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Escritura</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para cortar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Herramientas para pintar</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Manualidades</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Microporoso y corrospum</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Papeles</a></li>
                                <li><a href="categorias.php" class="color-internas underlineHover">Pinturas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <!-- End Main Menu -->
</header>

<!-- Start Menu Otros -->
<div id="menu-otros" class="menu-otros bg-white">
    <button type="button" class="btn--volver bg-primary color-white otros-link text-may" name="volver">
        <span>Volver</span>
        <i class="icon-arrow-left"></i>
    </button>
    <a href="nosotros.php" class="submenu--item">
        <i class="icon-logo color-internas"></i>
        <h3 class="h3 text-may color-internas">Nosotros</h3>
    </a>
    <a href="servicios.php" class="submenu--item">
        <i class="icon-nuestros-servicios color-internas"></i>
        <h3 class="h3 text-may color-internas">Nuestros Servicios</h3>
    </a>
    <a href="sucursales.php" class="submenu--item">
        <i class="icon-sucursales color-internas"></i>
        <h3 class="h3 text-may color-internas">Sucursales</h3>
    </a>
    <a href="contactenos.php" class="submenu--item">
        <i class="icon-contacto color-internas"></i>
        <h3 class="h3 text-may color-internas">Contáctanos</h3>
    </a>
</div>
<!-- End Menu Otros -->

<!-- Start Nuestras Tiendas -->
<a href="sucursales.php" class="nuestrasTiendas-fixed">
    <div class="nuestrasTiendas__icon">
        <img class="location-house" src="assets/images/location-house.png" alt="Ubicación"/>
        <img class="location-mapa" src="assets/images/location-mapa.png" alt="Ubicación"/>
    </div>
    <div class="nuestrasTiendas__caption bg-gray">
        <p class="color-white">Nuestras <br> Tiendas</p>
    </div>
</a>
<!-- End Nuestras Tiendas -->