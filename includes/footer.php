<footer id="footer" class="footer">
    <div class="footer--top bg-gray">
        <div class="container">
            <div class="grid-footer">
                <!-- Start Servicio al cliente -->
                <div class="footer-col wow fadeInRight" data-wow-duration="1s" data-wow-delay="0s">
                    <h5 class="h5 text-may color-white">
                        <i class="icon-arrow-right"></i>
                        <span>Servicio al cliente</span>
                    </h5>
                    <ul class="footer-list">
                        <li><a href="como-comprar.php" class="footer-list__link underlineHover">Como comprar</a></li>
                        <li><a href="preguntas-frecuentes.php" class="footer-list__link underlineHover">Preguntas frecuentes</a></li>
                        <li><a href="formas-de-pago.php" class="footer-list__link underlineHover">Formas de pago</a></li>
                        <li><a href="politica-de-entrega.php" class="footer-list__link underlineHover">Políticas de entrega</a></li>
                        <li><a href="cambios-y-devoluciones.php" class="footer-list__link underlineHover">Cambios y devoluciones</a></li>
                        <li><a href="terminos-y-condiciones.php" class="footer-list__link underlineHover">Términos y condiciones</a></li>
                        <li><a href="politicas-de-privacidad.php" class="footer-list__link underlineHover">Políticas de privacidad</a></li>
                    </ul>
                </div>
                <!-- End Servicio al cliente -->
                <!-- Start Footer Links -->
                <div class="footer-col wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s">
                    <ul class="footer-listIcon">
                        <li><a href="nosotros.php"><h5 class="h5 text-may color-white underlineHover">Nosotros</h5></a></li>
                        <li><a href="servicios.php"><h5 class="h5 text-may color-white underlineHover">Nuestros Servicios</h5></a></li>
                        <li><a href="sucursales.php"><h5 class="h5 text-may color-white underlineHover">Sucursales</h5></a></li>
                        <li><a href="contactenos.php"><h5 class="h5 text-may color-white underlineHover">Contáctanos</h5></a></li>
                    </ul>
                </div>
                <!-- End Footer Links -->
                <!-- Start Footer Links -->
                <div class="footer-col wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.4s">
                    <ul class="footer-listIcon">
                        <li>
                            <a href="#">
                                <h5 class="h5 text-may color-white">
                                    <i class="icon-comprobante"></i>
                                    <span class="underlineHover">Comprobantes electrónicos</span>
                                </h5>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h5 class="h5 text-may color-white">
                                    <i class="icon-plataforma-web"></i>
                                    <span class="underlineHover">Plataforma B2B</span>
                                </h5>
                            </a>
                        </li>
                        <li>
                            <a href="libro-de-reclamos.php">
                                <h5 class="h5 text-may color-white">
                                    <i class="icon-libro-reclamosF"></i>
                                    <span class="underlineHover">Libro de Reclamaciones</span>
                                </h5>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Footer Links -->
                <!-- Start Newsletter -->
                <div class="footer-col wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.6s">
                    <div class="newsletter">
                        <div class="newsletter--heading">
                            <i class="icon-message-color"><span class="path1"></span><span class="path2"></span></i>
                            <div class="newsletter--heading__caption">
                                <h5 class="text-may color-white">¡Recibe nuestras promociones!</h5>
                                <p class="color-white">Recibe actualizaciones por correo electrónico sobre nuestra tienda y ofertas especiales.</p>
                            </div>
                        </div>
                        <form class="form newsletter--form" action="#" method="post">
                            <div class="form__wrapper">
                                <input type="text" class="form__input bg-input" id="colegio" name="colegio">
                                <label class="form__label">
                                    <span class="form__label-content"></span>
                                </label>
                            </div>
                            <button class="btn btn-red text-may" type="submit" name="button">Suscríbete</button>
                        </form>
                    </div>
                </div>
                <!-- End Newsletter -->
            </div>
        </div>
    </div>

    <div class="footer--bot bg-white">
        <div class="container">
            <div class="footer-foot">
                <!-- Start Copyright -->
                <div class="copyright text-may wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.25s">
                    <p>Comercial LI</p>
                    <p>Todos los derechos reservados 2020</p>
                </div>
                <!-- End Copyright -->
                <!-- Start Payment -->
                <div class="payment">
                    <div class="payment-col wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.25s">
                        <img class="img-responsive" src="assets/images/visa.jpg" alt="Payment"/>
                        <img class="img-responsive" src="assets/images/mastercard.jpg" alt="Payment"/>
                        <img class="img-responsive" src="assets/images/american-express.jpg" alt="Payment"/>
                        <img class="img-responsive" src="assets/images/diners-club.jpg" alt="Payment"/>
                    </div>
                    <div class="payment-col wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.25s">
                        <img class="img-responsive" src="assets/images/verified-visa-banco.jpg" alt="Payment"/>
                        <img class="img-responsive" src="assets/images/verified-visa.jpg" alt="Payment"/>
                        <img class="img-responsive" src="assets/images/mastercard-securecode.jpg" alt="Payment"/>
                        <img class="img-responsive" src="assets/images/american-express-safekey.jpg" alt="Payment"/>
                    </div>
                </div>
                <!-- End Payment -->
                <!-- Start Powered -->
                <div class="powered text-right text-may wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.25s">
                    <p>Powered by <a href="http://exe.pe/">EXE</a></p>
                    <p><a href="https://validator.w3.org/check?uri=referer" target="_blank">HTML</a> · <a href="https://jigsaw.w3.org/css-validator/check/referer" target="_blank">CSS</a></p>
                </div>
                <!-- End Powered -->
            </div>
        </div>
    </div>
</footer>