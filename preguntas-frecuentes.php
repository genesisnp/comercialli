<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-frequent-questions">
        <section class="banner mg-b-banner">
            <img class="img-cover" src="assets/images/banner/preguntas-frecuentes.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-preg-frecuentes color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Preguntas frecuentes</h2>
            </div>
        </section>
        <section class="sct-acc-questions">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-10 float-center">
                        <ul class="accordion">
                            <li><a class="accordion-heading titles-int">¿Hay un monto mínimo de compra para que pueda ser enviado? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto quos obcaecati rerum! Laborum a asperiores modi quos obcaecati 
                                        rerum! Laborum a asperiores modi omnis quos obcaecati rerum! Laborum a asperiores modipraesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Cuál es el costo e envío? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Qué pasa si recibo un producto dañado? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Tengo seguridad al ingresar los datos de mi tarjeta de crédito? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Qué medios de pago puedo utilizar? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Cómo seque llegó conforme mi pedido? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Puedo modificar mi pedido? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading titles-int">¿Cómo puedo saber si hice mi compra correctamente? <i
                                        class="flecha-accord icon-arrow-left"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas font-s-p1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil in nisi, 
                                        minima dolore enim quos obcaecati rerum! Laborum a asperiores modi. Aspernatur nam sint, 
                                        ullam labore asperiores iusto omnis praesentium.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
