<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <!-- Start Registro Realizado -->
        <section class="section-otros recuperar-contrasena">
            <h3 class="h3 text-center text-may color-primary">Gracias Juan Perez</h3>
            <div class="container">
                <p class="text-center text-otros color-internas">Tu registro se realizó con éxito.</p>
                <p class="text-center text-otros color-internas">A continuación te enviaremos un correo de confirmación.</p>
                <div class="text-center">
                    <a href="index.php" class="btn btn-red">Ir al inicio</a>
                </div>
            </div>
        </section>
        <!-- End Registro Realizado -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
