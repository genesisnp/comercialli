<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/nosotros.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-contacto color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Contáctanos</h2>
            </div>
        </section>
        <!-- Start Gracias -->
        <section class="section-otros gracias">
            <div class="container-custom c-992">
                <h3 class="h3 text-center text-may color-primary">Iniciar sesión</h3>
                <p class="text-center text-otros color-internas">Tu mensaje se ha enviado correctamente.</p>
                <p class="text-center text-otros color-internas">Muy pronto nos pondremos en contacto contigo para darle la mejor asesoría y orientación de todas sus dudas.</p>
                <div class="text-center">
                    <a href="index.php" class="btn btn-red">Seguir comprando</a>
                </div>
            </div>
        </section>
        <!-- End Gracias -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
