<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main class="main-index">
        <!-- Start Slider -->
        <div class="slider">
            <div class="slide">
                <img class="img-cover" src="assets/images/slider/slide1.jpg" alt="Slide"/>
                <div class="slide-caption">
                    <div class="slide-caption__promo color-primary titles-black">
                        <p class="number">30</p>
                        <div class="slide-caption__promoDscto">
                            <p class="porcentaje">%</p>
                            <p class="dscto">dscto</p>
                        </div>
                    </div>
                    <p class="slide-caption__text titles-black">Impresora HP Ink</p>
                    <p class="slide-caption__text titles-black">Advantage Ultra 4729</p>
                    <a href="" class="slide-caption__btn btn btn-red">
                        <span class="text-may">Ver más</span>
                        <i class="icon-arrow-right color-white"></i>
                    </a>
                </div>
            </div>
            <div class="slide">
                <img class="img-cover" src="assets/images/slider/slide1.jpg" alt="Slide"/>
                <div class="slide-caption">
                    <div class="slide-caption__promo color-primary titles-black">
                        <p class="number">40</p>
                        <div class="slide-caption__promoDscto">
                            <p class="porcentaje">%</p>
                            <p class="dscto">dscto</p>
                        </div>
                    </div>
                    <p class="slide-caption__text titles-black">Impresora HP Ink</p>
                    <p class="slide-caption__text titles-black">Advantage Ultra 4729</p>
                    <a href="" class="slide-caption__btn btn btn-red">
                        <span class="text-may">Ver más</span>
                        <i class="icon-arrow-right color-white"></i>
                    </a>
                </div>
            </div>
            <div class="slide">
                <img class="img-cover" src="assets/images/slider/slide1.jpg" alt="Slide"/>
                <div class="slide-caption">
                    <div class="slide-caption__promo color-primary titles-black">
                        <p class="number">50</p>
                        <div class="slide-caption__promoDscto">
                            <p class="porcentaje">%</p>
                            <p class="dscto">dscto</p>
                        </div>
                    </div>
                    <p class="slide-caption__text titles-black">Impresora HP Ink</p>
                    <p class="slide-caption__text titles-black">Advantage Ultra 4729</p>
                    <a href="" class="slide-caption__btn btn btn-red">
                        <span class="text-may">Ver más</span>
                        <i class="icon-arrow-right color-white"></i>
                    </a>
                </div>
            </div>
            <div class="slide">
                <img class="img-cover" src="assets/images/slider/slide1.jpg" alt="Slide"/>
                <div class="slide-caption">
                    <div class="slide-caption__promo color-primary titles-black">
                        <p class="number">60</p>
                        <div class="slide-caption__promoDscto">
                            <p class="porcentaje">%</p>
                            <p class="dscto">dscto</p>
                        </div>
                    </div>
                    <p class="slide-caption__text titles-black">Impresora HP Ink</p>
                    <p class="slide-caption__text titles-black">Advantage Ultra 4729</p>
                    <a href="" class="slide-caption__btn btn btn-red">
                        <span class="text-may">Ver más</span>
                        <i class="icon-arrow-right color-white"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- End Slider -->

        <!-- Start Ofertas del Mes -->
        <section class="section-slider ofertas-mes">
            <div class="container">
                <div class="sub-title">
                    <i class="icon-ofertas color-primary"></i>
                    <h3 class="h3 color-internas">Ofertas del mes</h3>
                </div>
                
                <div class="slider-ofertas slider--arrows slider--dots">
                    <div class="producto wow zoomIn" data-wow-duration="1s" data-wow-delay="2s">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-30%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-01.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 80.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 69.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow zoomIn" data-wow-duration="1s" data-wow-delay="2.2s">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-15%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-02.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 80.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 20.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow zoomIn" data-wow-duration="1s" data-wow-delay="2.4s">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-10%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-03.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Memoria USB 32 gb (DT100/102)</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 10.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 5.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow zoomIn" data-wow-duration="1s" data-wow-delay="2.6s">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-10%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-04.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Base p/CPU (Cs003M) Ajustable c/Ruedas Y Frenos</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 15.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 12.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-30%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-01.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 80.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 69.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-15%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-02.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 80.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 20.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-10%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-03.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Memoria USB 32 gb (DT100/102)</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 10.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 5.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <span class="producto-image__dscto bg-primary color-white">-10%</span>
                            <a href="detalle-producto-ofertas.php">
                                <img class="img-cover" src="assets/images/ofertas/producto-04.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto-ofertas.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas text-may">Base p/CPU (Cs003M) Ajustable c/Ruedas Y Frenos</h4>
                            <div class="antes">
                                <span class="antes-text">Antes</span>
                                <span class="antes-number">S/ 15.00</span>
                            </div>
                            <div class="ahora color-primary">
                                <span class="ahora-text">Ahora</span>
                                <span class="ahora-number">S/ 12.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Ofertas del Mes -->

        <!-- Start Productos Destacados -->
        <section class="section productos-destacados">
            <div class="container">
                <div class="productos-destacados--heading wow fadeInDown" data-wow-duration="1s" data-wow-delay="0s">
                    <div class="sub-title">
                        <i class="icon-product-destacados color-primary"></i>
                        <h3 class="h3 color-internas">Productos Destacados</h3>
                    </div>
                    <div class="tabs">
                        <a href="javascript:void(0)" class="tab-item current" data-tab="navidad">Navidad</a>
                        <a href="javascript:void(0)" class="tab-item" data-tab="escritorio">Escritorio</a>
                        <a href="javascript:void(0)" class="tab-item" data-tab="oficina">Oficina</a>
                        <a href="javascript:void(0)" class="tab-item" data-tab="juguetes">Juguetes</a>
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-panel current" id="navidad">
                        <div class="slider-productosDestacados slider--arrows slider--dots">
                            <div class="producto wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.4s">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.6s">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.8s">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto wow fadeInRight" data-wow-duration="1s" data-wow-delay="1s">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-panel" id="escritorio">
                        <div class="slider-productosDestacados slider--arrows slider--dots">
                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-panel" id="oficina">
                        <div class="slider-productosDestacados slider--arrows slider--dots">
                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-panel" id="juguetes">
                        <div class="slider-productosDestacados slider--arrows slider--dots">
                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Parlantes Xtreme Usb (Kes-215A)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 12.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Juego Didáctico Bloques Logicos Grande Envase x48</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 5.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Mouse Xtreme Mini Optical Usb Retractil (Km0-130R)</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 10.00</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="producto">
                                <div class="producto-image">
                                    <a href="detalle-producto.php">
                                        <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                                    </a>

                                    <div class="product-overlay">
                                        <a href="detalle-producto.php">Ver detalle</a>
                                    </div>
                                </div>
                                
                                <div class="product-body">
                                    <h4 class="h4 color-internas text-may">Portadocumento de Cartón (37784) con Liga Color Só</h4>
                                    <div class="ahora color-primary">
                                        <span class="ahora-number">S/ 8.50</span>
                                    </div>
                                </div>

                                <div class="producto-foot">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a href="" class="btn btn-red">
                                            <i class="icon-carrito color-white"></i>
                                            <span>Comprar</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Productos Destacados -->

        <!-- Start Servicios -->
        <section class="section tramado">
            <div class="container">
                <h3 class="h3-big color-internas text-center">Nuestros servicios</h3>

                <div class="slider-homeServicios slider--arrows slider--dots">
                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
                        <a href="servicios.php" class="home-servicio--item">
                            <!-- Heading -->
                            <div class="home-servicio--item__heading">
                                <i class="icon-delivery"></i>
                            </div>
                            <!-- Content-->
                            <div class="home-servicio--item__content">
                                <h4 class="title text-may">Delivery</h4>
                                <p class="caption color-gray">Trasladamos tus pedidos día a día a cientos de familias enfocados en que el producto llegue hasta su destino a tiempo y con el cuidado que requiere.</p>
                            </div>
                        </a>
                    </div>

                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                        <a href="servicios.php" class="home-servicio--item">
                            <!-- Heading -->
                            <div class="home-servicio--item__heading">
                                <i class="icon-credito-coorporativo"></i>
                            </div>
                            <!-- Content-->
                            <div class="home-servicio--item__content">
                                <h4 class="title text-may">Crédito Corporativo</h4>
                                <p class="caption color-gray">Trasladamos tus pedidos día a día a cientos de familias enfocados en que el producto llegue hasta su destino a tiempo y con el cuidado que requiere.</p>
                            </div>
                        </a>
                    </div>

                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                        <a href="servicios.php" class="home-servicio--item">
                            <!-- Heading -->
                            <div class="home-servicio--item__heading">
                                <i class="icon-pedidos"></i>
                            </div>
                            <!-- Content-->
                            <div class="home-servicio--item__content">
                                <h4 class="title text-may">Pedidos con Contrato</h4>
                                <p class="caption color-gray">Trasladamos tus pedidos día a día a cientos de familias enfocados en que el producto llegue hasta su destino a tiempo y con el cuidado que requiere.</p>
                            </div>
                        </a>
                    </div>

                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                        <a href="servicios.php" class="home-servicio--item">
                            <!-- Heading -->
                            <div class="home-servicio--item__heading">
                                <i class="icon-vales"></i>
                            </div>
                            <!-- Content-->
                            <div class="home-servicio--item__content">
                                <h4 class="title text-may">Vales de Consumo</h4>
                                <p class="caption color-gray">Trasladamos tus pedidos día a día a cientos de familias enfocados en que el producto llegue hasta su destino a tiempo y con el cuidado que requiere.</p>
                            </div>
                        </a>
                    </div>

                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                        <a href="servicios.php" class="home-servicio--item">
                            <!-- Heading -->
                            <div class="home-servicio--item__heading">
                                <i class="icon-serv-personalizado"></i>
                            </div>
                            <!-- Content-->
                            <div class="home-servicio--item__content">
                                <h4 class="title text-may">Servicio Personalizado</h4>
                                <p class="caption color-gray">Trasladamos tus pedidos día a día a cientos de familias enfocados en que el producto llegue hasta su destino a tiempo y con el cuidado que requiere.</p>
                            </div>
                        </a>
                    </div>

                    <div class="item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <a href="servicios.php" class="home-servicio--item">
                            <!-- Heading -->
                            <div class="home-servicio--item__heading">
                                <i class="icon-atenc-cole"></i>
                            </div>
                            <!-- Content-->
                            <div class="home-servicio--item__content">
                                <h4 class="title text-may">Atención a Colegios</h4>
                                <p class="caption color-gray">Trasladamos tus pedidos día a día a cientos de familias enfocados en que el producto llegue hasta su destino a tiempo y con el cuidado que requiere.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Servicios -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Cotizar -->
    <div id="cotizar" class="cotizar text-center bg-white">
        <a id="cerrar-cotizar" href="javascript:void(0)" class="cerrar btn-red">
            <span class="line line1"></span>
            <span class="line line2"></span>
        </a>
        <h4 class="color-primary">Envíanos tu lista escolar</h4>
        <p class="color-internas">Aprovecha mejor tu tiempo envíanos tu lista de útiles escolares online, nosotros lo armamos y te lo llevamos a tu casa.</p>
        <form class="form cotizar--form" action="#" method="post">
            <div class="form__wrapper">
                <input type="text" class="form__input bg-input" id="colegio" name="colegio">
                <label class="form__label">
                    <span class="form__label-content">Colegio</span>
                </label>
            </div>
            <div class="form__wrapper">
                <select class="form__select bg-input">
                    <option selected>Distrito Colegio</option>
                    <option value="1">Ancón</option>
                    <option value="2">Ate</option>
                    <option value="3">Barranco</option>
                    <option value="4">Breña</option>
                </select>
            </div>
            <div class="form__wrapper">
                <select class="form__select bg-input">
                    <option selected>Nivel y Grado</option>
                    <option value="1">1º A</option>
                    <option value="2">1º B</option>
                    <option value="3">2º A</option>
                    <option value="4">2º B</option>
                </select>
            </div>
            <button class="btn btn-red text-may" type="submit" name="button">Cotizar</button>
        </form>
    </div>
    <!-- End Cotizar -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
