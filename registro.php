<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <!-- Start Registro -->
        <section class="section-otros registro">
            <div class="container-custom c-768">
                <h3 class="h3 text-center text-may color-primary">Registro</h3>
                <div class="box-form">
                    <form class="form" action="#" method="post">
                        <div class="grid">
                            <div class="col-2">
                                <div class="form__wrapper">
                                    <input type="text" class="form__input bg-input" id="nombre" name="nombre">
                                    <label class="form__label">
                                        <span class="form__label-content">Nombre</span>
                                    </label>
                                </div>
                                <div class="form__wrapper">
                                    <input type="text" class="form__input bg-input" id="dni" name="dni">
                                    <label class="form__label">
                                        <span class="form__label-content">Documento de identidad:</span>
                                    </label>
                                </div>
                                <div class="form__wrapper">
                                    <input type="tel" class="form__input bg-input" id="telefono" name="telefono">
                                    <label class="form__label">
                                        <span class="form__label-content">Teléfono</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form__wrapper">
                                    <input type="text" class="form__input bg-input" id="apellido" name="apellido">
                                    <label class="form__label">
                                        <span class="form__label-content">Apellido</span>
                                    </label>
                                </div>
                                <div class="form__wrapper">
                                    <input type="tel" class="form__input bg-input" id="celular" name="celular">
                                    <label class="form__label">
                                        <span class="form__label-content">Celular</span>
                                    </label>
                                </div>
                                <div class="form__wrapper">
                                    <input type="email" class="form__input bg-input" id="email" name="email">
                                    <label class="form__label">
                                        <span class="form__label-content">Correo</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="checkbox">
                            <label class="label-pol">
                                <input type="checkbox"/><i class="helper"></i>
                                <span>He leído y acepto los <a href="terminos-y-condiciones.php" class="span-pol titles-int color-primary">Términos y Condiciones</a> y la <a href="politicas-de-privacidad.php" class="span-pol titles-int color-primary">Política de Privacidad.</a></span>
                            </label>
                            <label class="label-pol">
                                <input type="checkbox"/><i class="helper"></i>
                                <span>Autorizo recibir publicidad, promociones, ofertas y encuestas de Comercial Li</span>
                            </label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-registro btn-red text-may" name="button">Registrarme</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- End Registro -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
