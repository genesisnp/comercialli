<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-camb-dev">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/nosotros.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-contacto color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Contáctanos</h2>
            </div>
        </section>
        <section class="sct-form-contact">
            <div class="container">
                <div class="row d-alig-flex flex-column">
                    <p class="p-internas text-center col-xs-12 p-form">Si tiene alguna duda y/o sugerencia dejanos un mensaje y en breve te estaremos respondiendo.</p>
                    <form action="#" class="form col-xs-11 col-sm-10 wow fadeInUp" method="post">
                        <div class="row d-flex-just">
                            <div class="col-xs-11 col-sm-10">
                                <div class="row">
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="name-contac" name="name-contac">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="last-name-contac" name="last-name-contac">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="dni-contac" name="dni-contac">
                                        <label class="form__label">
                                            <span class="form__label-content">DNI</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="email" class="form__input bg-input" id="email-contac" name="email-contac">
                                        <label class="form__label">
                                            <span class="form__label-content">E-mail</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="email" class="form__input bg-input" id="phone-contac" name="phone-contac">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="cel-contac" name="cel-contac">
                                        <label class="form__label">
                                            <span class="form__label-content">Celular</span>
                                        </label>
                                    </div>                                   
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="message-contac"
                                            name="message-contac"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="checkbox">
                                            <label class="label-pol">
                                                <input type="checkbox" /><i class="helper"></i><span>He leído y acepto
                                                    la <a href="politicas-de-privacidad.php" class="span-pol titles-int color-primary">Política de
                                                        Privacidad.</a></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 d-flex-just">
                                        <button type="submit" class="btn-send btn-red titles-int "
                                                id="btn-send-form">ENVIAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>

