<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <!-- Start Detalle Producto -->
        <section class="section detalleProducto">
            <div class="container">
                <!-- Start Breadcrumb -->
                <div class="breadcrumb">
                    <span class="breadcrumb-item">Productos</span>
                    <span class="breadcrumb-item">»</span>
                    <span class="breadcrumb-item">Cómputo & Tecnología</span>
                    <span class="breadcrumb-item">»</span>
                    <span class="breadcrumb-item">Accesorios de Cómputo</span>
                    <span class="breadcrumb-item">»</span>
                    <span class="breadcrumb-item active">Parlantes</span>
                </div>
                <!-- End Breadcrumb -->
                <!-- Start View -->
                <div class="grid-detalle">
                    <div class="detalleProducto-info">
                        <div class="detalleProducto-info--heading wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                            <h3 class="color-internas">Audífonos on Ear Beats solo3, con Bluetooth</h3>
                            <p class="codigo color-primary">Código: 137295</p>
                        </div>
                        <div class="zoom-gallery wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0s">
                            <div class="view">
                                <img class="xzoom img-cover" src="assets/images/detalle-producto/original/detalle-01.jpg" xoriginal="assets/images/detalle-producto/original/detalle-01.jpg" alt="Imagen Principal">
                            </div>
                            <div class="thumbnails">
                                <a href="assets/images/detalle-producto/original/detalle-01.jpg">
                                    <div class="xzoom__thumb">
                                        <img class="img-responsive" src="assets/images/detalle-producto/preview/detalle-01.jpg" alt="Imagen Thumb" title="Ampliando Imagen">
                                    </div>
                                </a>
                                <a href="assets/images/detalle-producto/original/detalle-02.jpg">
                                    <div class="xzoom__thumb">
                                        <img class="img-responsive" src="assets/images/detalle-producto/preview/detalle-02.jpg" alt="Imagen Thumb" title="Ampliando Imagen">
                                    </div>
                                </a>
                                <a href="assets/images/detalle-producto/original/detalle-03.jpg">
                                    <div class="xzoom__thumb">
                                        <img class="img-responsive" src="assets/images/detalle-producto/preview/detalle-03.jpg" alt="Imagen Thumb" title="Ampliando Imagen">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="detalleProducto-info--body">
                            <div class="precio-flex wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">
                                <p class="ahora-number color-primary">S/ 69.00</p>
                            </div>
                            <!-- Start Información del Producto -->
                            <button id="btn-info" type="button" class="btn-detalleCollapse color-internas bg-white">
                                <i class="icon-arrow-right"></i>
                                <span>Información del producto</span>
                            </button>
                            <div class="infoProducto wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                                <div class="infoProducto-content">
                                    <div class="amount-buy">
                                        <div class="number">
                                            <span class="minus">-</span>
                                            <input type="text" value="1"/>
                                            <span class="plus">+</span>
                                        </div>
                                        <a id="comprarFixed" href="carrito-de-compras.php" class="btn btn-red comprarFixed">
                                            <i class="icon-carrito color-white"></i>
                                            <span class="text-may">Comprar</span>
                                        </a>
                                    </div>
                                    <p class="color-internas">Dimensiones 80 x 96 x 96 mm, no necesitan un adaptador externo o baterias adicionales para funcionar.</p>
                                </div>
                            </div>
                            <!-- End Información del Producto -->
                            <!-- Start Color -->
                            <div class="radio-flex colorOptions wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.6s">
                                <h5 class="text-may color-internas">Color:</h5>
                                <div class="color-inline">
                                    <div class="radio-color">
                                        <input id="color-amarillo" type="radio" name="color" checked>
                                        <label for="color-amarillo">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="radio-color">
                                        <input id="color-rojo" type="radio" name="color">
                                        <label for="color-rojo">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="radio-color">
                                        <input id="color-verde" type="radio" name="color">
                                        <label for="color-verde">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="radio-color">
                                        <input id="color-gris" type="radio" name="color">
                                        <label for="color-gris">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- End Color -->
                            <!-- Start Especificaciones -->
                            <button id="btn-esp" type="button" class="btn-detalleCollapse color-internas bg-white">
                                <i class="icon-arrow-right"></i>
                                <span>Especificaciones</span>
                            </button>
                            <form class="detalleProducto--form wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.8s" action="" method="post">
                                <!-- Start Tipo de Servicio -->
                                <div class="radio-flex">
                                    <div class="tipo-servicio">
                                        <h5 class="text-may color-internas">Servicio de armado:</h5>
                                        <div class="radio-inline">
                                            <div class="radio">
                                                <input type="radio" id="siArmado" name="servicio-armado" checked>
                                                <label for="siArmado">Si (S/ 15)</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" id="noArmado" name="servicio-armado">
                                                <label for="noArmado">No</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tipo-servicio">
                                        <h5 class="text-may color-internas">Servicio de envoltura:</h5>
                                        <div class="radio-inline">
                                            <div class="radio">
                                                <input type="radio" id="siEnvoltura" name="servicio-envoltura" checked>
                                                <label for="siEnvoltura">Si (S/ 15)</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" id="noEnvoltura" name="servicio-envoltura">
                                                <label for="noEnvoltura">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Tipo de Servicio -->
                            </form>
                            <!-- End Especificaciones -->
                        </div>
                    </div>
                </div>
                <!-- End View -->
            </div>
        </section>
        <!-- End Detalle Producto -->

        <!-- Start Productos Relacionados -->
        <section class="section productosRelacionados">
            <div class="container">
                <h3 class="h3 color-internas">Productos Relacionados</h3>
                
                <div class="slider-productosRelacionados slider--arrows slider--dots">
                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos Con Memoria Interna <br> NW-WS413 Verde</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 349.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <!--
                                    <i class="icon-carrito color-white carritoHover"></i>
                                    -->
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlante Flip 4 Acuático</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 399.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlantes Inalámbricos SoundLink <br> Color II Rojo</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 599.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos In-Ear Con Micrófono <br> MDR-EX15AP Morado</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 38.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-01.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos Con Memoria Interna <br> NW-WS413 Verde</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 349.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <!--
                                    <i class="icon-carrito color-white carritoHover"></i>
                                    -->
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-02.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlante Flip 4 Acuático</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 399.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-03.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Parlantes Inalámbricos SoundLink <br> Color II Rojo</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 599.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="producto">
                        <div class="producto-image">
                            <a href="detalle-producto.php">
                                <img class="img-cover" src="assets/images/productos-destacados/producto-04.jpg" alt="Slide"/>
                            </a>

                            <div class="product-overlay">
                                <a href="detalle-producto.php">Ver detalle</a>
                            </div>
                        </div>
                        
                        <div class="product-body">
                            <h4 class="h4 color-internas">Audífonos In-Ear Con Micrófono <br> MDR-EX15AP Morado</h4>
                            <div class="ahora color-primary">
                                <span class="ahora-number">S/ 38.00</span>
                            </div>
                        </div>

                        <div class="producto-foot">
                            <div class="amount-buy">
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="1"/>
                                    <span class="plus">+</span>
                                </div>
                                <a href="" class="btn btn-red">
                                    <i class="icon-carrito color-white"></i>
                                    <span>Comprar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Productos Relacionados -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
