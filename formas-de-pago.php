<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-frm-pag">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/formas-de-pago.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-formas-de-pago color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Formas de pago</h2>
            </div>
        </section>
        <section class="forms-pago">
            <div class="container flex-mob">
                <div class="row d-flex-just">
                    <div class="card-form-pag col-xs-12 col-md-5 wow zoomIn" data-wow-delay="1s">
                        <div class="row">
                            <div class="col-xs-12 col-md-10">
                                <div class="content-card-repeat">
                                    <div class="content-icon-card">
                                        <i class="icon-tabs icon-met-pago"></i>
                                    </div>
                                    <h2 class="ttl-tab text-uppercase titles-big color-internas">tarjeta de crédito / débito</h2>
                                    <p class="p-internas">Realiza el pago de tu compra con tarjeta de crédito / tarjeta de débito al instante.</p>
                                </div>
                                <div class="imgs-logos-fp">
                                    <img src="assets/images/logos/visa.jpg" alt="" class="img-cover">
                                    <img src="assets/images/logos/mastercard.jpg" alt="" class="img-cover">
                                    <img src="assets/images/logos/american.jpg" alt="" class="img-cover">
                                    <img src="assets/images/logos/diners.jpg" alt="" class="img-cover">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-form-pag col-xs-12 col-md-5 wow zoomIn" data-wow-delay="2s">
                        <div class="row">
                            <div class="col-xs-12 col-md-10 text-center">
                                <div class="content-card-repeat">
                                    <div class="content-icon-card">
                                        <i class="icon-tabs icon-dep-bancario"></i>
                                    </div>
                                    <h2 class="ttl-tab text-uppercase titles-big color-internas">tarjeta de crédito / débito</h2>
                                    <p class="p-internas">Realiza el pago de tu compra con tarjeta de crédito / tarjeta de débito al instante.</p>
                                </div>
                                <p class="titles-int">Banco BCP S/ 191-0000000000000</p>
                                <p class="titles-int">Banco BBVA S/ 191-0000000000000</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>

