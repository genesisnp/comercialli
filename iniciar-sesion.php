<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->

    <!-- Start Main -->
    <main>
        <!-- Start Iniciar Sesión -->
        <section class="section-otros iniciar-sesion">
            <div class="container-custom c-425">
                <h3 class="h3 text-center text-may color-primary">Iniciar Sesión</h3>
                <div class="box-form">
                    <form class="form" action="#" method="post">
                        <div class="form__wrapper">
                            <input type="text" class="form__input bg-input" id="nombre" name="nombre">
                            <label class="form__label">
                                <span class="form__label-content">Nombre</span>
                            </label>
                        </div>
                        <div class="form__wrapper">
                            <input type="text" class="form__input bg-input" id="usuario" name="usuario">
                            <label class="form__label">
                                <span class="form__label-content">Usuario</span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-login btn-red text-may" name="button">Ingresar</button>
                        <div class="text-center">
                            <a href="recuperar-contrasena.php" class="text-otros color-internas underlineHover">¿Olvidó su contraseña?</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- End Iniciar Sesión -->
    </main>
    <!-- End Main -->

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
