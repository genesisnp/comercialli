<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-lr">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/nosotros.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-libro-reclamos color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Libro de reclamos</h2>
            </div>
        </section>
        <section class="form-libro-reclamos">
            <div class="container">
                <div class="row d-alig-flex flex-column">
                    <div class="col-xs-12 col-sm-10 col-lg-7 text-center dscrp-lr">
                        <p class="p-internas font-s-p1 mb-2">Conforme a lo establecido en el Código de Protección y Defensa del Consumidor
                             este establecimiento cuenta con un Libro de Reclamaciones a tu disposición.
                        </p>
                        <p class="p-internas font-s-p1">Hoja de reclamación N°: <span class="titles-int">2020-00000001</span><br>
                        Fecha: 28-01-2020<br>
                        Hora: 11:36:09 am
                        </p>
                    </div>
                    <form action="#" class="form col-xs-11 col-sm-10" method="post">
                        <div class="row d-flex-just">
                            <div class="col-xs-12 col-md-10">
                                <div class="row">
                                    <h2 class="ttl-forms-lr text-uppercase color-internas col-xs-12">Datos del comprador</h2>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="text" class="form__input bg-input" id="name" name="name">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="text" class="form__input bg-input" id="last-name" name="last-name">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="text" class="form__input bg-input" id="domicilio" name="domicilio">
                                        <label class="form__label">
                                            <span class="form__label-content">Domicilio</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="email" class="form__input bg-input" id="email" name="email">
                                        <label class="form__label">
                                            <span class="form__label-content">E-mail</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="email" class="form__input bg-input" id="phone" name="phone">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="text" class="form__input bg-input" id="phone-opc" name="phone-opc">
                                        <label class="form__label">
                                            <span class="form__label-content">Otro teléfono (opcional)</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="text" class="form__input bg-input" id="dni" name="dni">
                                        <label class="form__label">
                                            <span class="form__label-content">DNI</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-md-6">
                                        <input type="text" class="form__input bg-input" id="padres" name="padres">
                                        <label class="form__label">
                                            <span class="form__label-content">Madre o Padre (solo menores de edad)</span>
                                        </label>
                                    </div>
                                    <!-- <div class="col-xs-12 line-divider"></div> -->
                                    <h2 class="ttl-forms-lr text-uppercase color-internas col-xs-12">Sobre el producto o servicio</h2>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="radio-inline">
                                            <div class="radio">
                                                <input type="radio" id="productoA" name="sobre-producto" checked>
                                                <label for="productoA">Producto</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" id="servicioB" name="sobre-producto">
                                                <label for="servicioB">Servicio</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="n-pedido" name="n-pedido">
                                        <label class="form__label">
                                            <span class="form__label-content">Número de pedido</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="message-prod"
                                            name="message-prod"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                    <!-- <div class="line-divider col-xs-12"></div> -->
                                    <h2 class="ttl-forms-lr text-uppercase color-internas col-xs-12">Detalles del reclamo</h2>
                                    <div class="col-xs-12">
                                        <div class="radio-inline">
                                            <div class="radio">
                                                <input type="radio" id="quejaA" name="detalle-reclamo" checked>
                                                <label for="quejaA">Queja</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" id="reclamoB" name="detalle-reclamo">
                                                <label for="reclamoB">Reclamo</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="message-reclamo"
                                            name="message-reclamo"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="checkbox">
                                            <label class="label-pol">
                                                <input type="checkbox" /><i class="helper"></i><span>He leído y acepto
                                                    la <a href="politicas-de-privacidad.php" class="span-pol titles-int color-primary">Política de
                                                        Privacidad.</a></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 d-flex-just">
                                        <button type="submit" class="btn-send btn-red titles-int "
                                                id="btn-send-form">ENVIAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>

