<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-fcomo-comprar">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/politica-de-entrega.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-pol-entrega color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Política de entrega</h2>
            </div>
        </section>
        <section class="sct-info-pe">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row d-alig-flex">
                            <div class="col-xs-7 pd-x-0 hidden-xs hidden-sm">
                                <div class="content-img-pe">
                                    <img class="img-cover wow fadeInLeft" src="assets/images/internas/car.jpg" alt="">
                                    <img class="img1-png-pe wow zoomIn" data-wow-delay="1s" src="assets/images/internas/person1.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <ul class="list-pe wow fadeInRight">
                                    <li class="item-list-pe p-internas font-s-p1">Todos los productos adquiridos en este Sitio Web se entregarán 
                                        directamente al domicilio que indique el cliente.</li>
                                    <li class="item-list-pe p-internas font-s-p1">Los pedidos se entregarán dentro de las 48 horas. No hay despachos los días domingos y feriados.</li>
                                    <li class="item-list-pe p-internas font-s-p1">Los cambios de dirección deben ser notificados de manera inmediata al correo marketing@comercial-li.com, 
                                        teniendo en cuenta que todo cambio de dirección está sujeto a costos adicionales que asumirá el cliente.</li>
                                    <li class="item-list-pe p-internas font-s-p1">La información del lugar de envío, como datos de contacto (correo y número telefónico) son completa responsabilidad del cliente. 
                                        Si hubiese un error en la dirección, el producto no podrá ser entregado.</li>
                                    <li class="item-list-pe p-internas font-s-p1">Los clientes recibirán un código de seguimiento para que se encuentren informados acerca del estado de su pedido.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 flex-mob ">
                        <div class="row d-alig-flex">
                            <div class="col-xs-12 col-md-5 d-flex-end">
                                <ul class="list-pe wow fadeInLeft">
                                    <li class="item-list-pe p-internas font-s-p1">Los plazo de entrega pueden ser afectados por razones ajenas a nosotros, como por ejemplo caso fortuito o de fuerza mayor 
                                        (Desastres naturales, carreteras intransitables y otros) solo se realizará dos visitas al domicilio indicado por el cliente, en caso no haya persona 
                                        (mayor de edad) que pueda recibir el producto en estas dos ocasiones, el pedido regresará a nuestras oficinas y el cliente tendrá que asumir los 
                                        costos adicionales de envío.</li>
                                    <li class="item-list-pe p-internas font-s-p1">Una vez entregado el pedido, pasado 2 días laborables no se aceptarán cambios ni devolución.</li>
                                    <li class="item-list-pe p-internas font-s-p1">En caso exista algún inconveniente con su pedido, el cliente deberá comunicarse de manera inmediata al correo marketing@comercial-li.com, 
                                        contando con 2 días hábiles una vez recibido el producto.</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-7 pd-x-0">
                                <div class="content-img-pe float-right">
                                    <img class="img-cover wow fadeInRight" src="assets/images/internas/pared.jpg" alt="">
                                    <img class="img2-png-pe wow zoomIn" data-wow-delay="1s" src="assets/images/internas/person2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>

