<!DOCTYPE html>
<html lang="es">
<!-- Start Head -->
<?php
    include 'includes/head.php'
?>
<!-- End Head -->
<body>

    <!-- Start Header -->
    <?php
        include 'includes/header.php'
    ?>
    <!-- End Header -->
    <main class="main-us">
        <section class="banner">
            <img class="img-cover" src="assets/images/banner/nosotros.jpg" alt="">
            <div class="content-ttl-banner">
                <i class="icon-banner icon-nuestros-servicios color-white"></i>
                <h2 class="ttl-banner color-white titles-big">Nosotros</h2>
            </div>
        </section>
        <!-- SECCION INTRO US -->
        <section class="sct-intro-us bg-forma">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 wow fadeIn">
                        <p class="sem-bold text-us text-center font-s-p1 color-internas">Una de las mejores empresas del sector de útiles de 
                        oficina y escolares (School and Office), donde puedes encontrar todo lo 
                        que necesitas para equipar, organizar tu oficina y toda la lista escolar 
                        en un sólo lugar. <span class="titles-black">Comercial Li S.A. </span>cuenta con el mejor servicio 
                        personalizado al cliente del sector y ofrece soluciones integrales que 
                        mejoran la productividad en la escuela y el trabajo.</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <p class="p-internas font-s-p1 wow fadeIn"><span class="titles-int">Comercial Li S.A.</span> es una empresa 100% peruana con más de 40 años de experiencia en el 
                            sector retail y ventas corporativas. Contamos con 11 tiendas en las zonas más accesibles de Lima y atendemos 
                            directamente a los colegios y empresas más exigentes del Perú, a quienes les hemos demostrado nuestro alto 
                            nivel de calidad en productos y servicios que les permiten reducir tiempos y costos.</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <p class="p-internas font-s-p1 wow fadeIn">Así­ como el mundo y las necesidades del cliente cambian, la gama de productos, la logí­stica y el servicio
                             personalizado de <span class="titles-int">Comercial Li S.A.</span> ha evolucionado para formar una solución integral que excede las expectativas de nuestros 
                             clientes.</p>
                        <p class="p-internas font-s-p1 wow fadeIn">Hoy, <span class="titles-int">Comercial Li S.A.</span > ha crecido responsable y sotenidamente con un compromiso social que contribuye al desarrollo sostenible del país.</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- SECCION MISION / VISION -->
        <section class="sct-mv">
            <div class="container">
                <div class="row">
                    <!-- mision -->
                    <div class="col-xs-12 col-md-6">
                        <div class="content-div-mv wow zoomIn">
                            <div class="content-dscrip-mv color-white">
                                <h2 class="ttl-mv titles-big">Misión</h2>
                                <p class="p-internas color-white mb-0 font-s-p1">Contribuir al mejoramiento de la 
                                    productividad de nuestros clientes, ofreciendo el mejor sistema de 
                                    distribución, servicio y asesorí­a profesional.</p>
                            </div>
                            <div class="content-img-mv">
                                <img class="img-cover" src="assets/images/internas/mision.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- vision -->
                    <div class="col-xs-12 col-md-6">
                        <div class="content-div-mv wow zoomIn">
                            <div class="content-dscrip-mv color-white">
                                <h2 class="ttl-mv titles-big">Visión</h2>
                                <p class="p-internas color-white mb-0 font-s-p1">Ser lí­der en abastecimiento de útiles de oficina y 
                                    escolares, superando las expectativas de nuestros clientes y creciendo sostenidamente 
                                    de acuerdo a nuestros valores.</p>
                            </div>
                            <div class="content-img-mv">
                                <img class="img-cover" src="assets/images/internas/vision.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- SECCION VALORES -->
        <section class="sct-values bg-forma">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row d-flex-just text-center">
                            <div class="col-xs-12 col-md-6 wow zoomIn">
                                <h2 class="ttl-sct-values titles-big color-internas">Valores</h2>
                                <p class="p-internas font-s-p1">La confianza y credibilidad que hemos ganado tanto de 
                                    nuestros clientes como de nuestros proveedores ha sido gracias a los valores que seguimos.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <div class="card-values">
                            <div class="content-card-repeat wow zoomIn" >
                                <div class="content-icon-card">
                                    <i class="icon-tabs icon-honestidad"></i>
                                </div>
                                <h2 class="ttl-tab text-uppercase titles-int color-internas">Honestidad</h2>
                            </div>
                            <div class="content-card-repeat wow zoomIn" data-wow-delay="0.5s">
                                <div class="content-icon-card">
                                    <i class="icon-tabs icon-responsabilidad"></i>
                                </div>
                                <h2 class="ttl-tab text-uppercase titles-int color-internas">Responsabilidad</h2>
                            </div>
                            <div class="content-card-repeat wow zoomIn" data-wow-delay="1s">
                                <div class="content-icon-card">
                                    <i class="icon-tabs icon-puntualidad"></i>
                                </div>
                                <h2 class="ttl-tab text-uppercase titles-int color-internas">Puntualidad</h2>
                            </div>
                            <div class="content-card-repeat wow zoomIn" data-wow-delay="1.5s">
                                <div class="content-icon-card">
                                    <i class="icon-tabs icon-serv-personalizado"></i>
                                </div>
                                <h2 class="ttl-tab text-uppercase titles-int color-internas">Vocación al consumidor</h2>
                            </div>
                            <div class="content-card-repeat wow zoomIn" data-wow-delay="2s">
                                <div class="content-icon-card">
                                    <i class="icon-tabs icon-innovacion"></i>
                                </div>
                                <h2 class="ttl-tab text-uppercase titles-int color-internas">innovación</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- SECCION RESPONSABILIDAD SOCIAL -->
        <section class="resp-social">
            <div class="container-fluid flex-mob">
                <div class="row d-alig-flex">
                    <div class="col-xs-12 col-md-6 pd-x-0 wow fadeInLeft">
                        <div class="slider-resp-social">
                            <div class="item-resp-social">
                                <img class="img-cover" src="assets/images/internas/slider-resp-soc1.jpg" alt="">
                                <div class="descr-slide">
                                    <p class="p-internas color-white">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Asperiores quis, dolorum amet necessitatibus totam, iusto non ducimus cumque minima quibusdam, tenetur at voluptates odit eos! Beatae dolorem sed ducimus nihil?</p>
                                </div>
                            </div>
                            <div class="item-resp-social">
                                <img class="img-cover" src="assets/images/internas/mision.jpg" alt="">
                                <div class="descr-slide">
                                    <p class="p-internas color-white">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Asperiores quis, dolorum amet necessitatibus totam, iusto non ducimus cumque minima quibusdam, tenetur at voluptates odit eos! Beatae dolorem sed ducimus nihil?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 wow fadeInRight">
                        <div class="descr-resp-social">
                            <h2 class="titles-big color-int color-internas">Responsabilidad<br>Social</h2>
                            <p class="p-internas font-s-p1">Comercial Li S.A. tiene una destacada ética empresarial y un compromiso social que contribuye al desarrollo sostenible del paí­s.</p>
                            <p class="p-internas font-s-p1">Comercial Li S.A. no sólo ha crecido respetando las normas para el beneficio de la sociedad en general, si no que toma decisiones 
                                analizando el impacto en la salud de las personas que adquieren los productos y el impacto ambiental de las acciones de la empresa.</p>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Start Footer -->
    <?php
        include 'includes/footer.php'
    ?>
    <!-- End Footer -->

    <!-- Start Scripts -->
    <?php
        include 'includes/scripts.php'
    ?>
    <!-- End Scripts -->

</body>
</html>
